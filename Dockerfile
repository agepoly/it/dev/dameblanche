FROM debian:jessie
MAINTAINER informatique@agepoly.ch
ARG version=0.2
ARG project_name="dameblanche"

RUN echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections

RUN apt-get update
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8

ENV DEBIAN_FRONTEND noninteractive
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US
ENV LC_ALL en_US.UTF-8

RUN apt-get update
RUN apt-get install -y \
    apache2 \  
    git \
    libapache2-mod-php5 \
    php5-common \
    php5-mysql


## If you build from git repo uncomment this block
#ARG git_branch="master"
#ARG gitlab_token_name="gitlab+deploy-token-132152"
#ARG gitlab_token_password="vad83RGyBhJC9qLngUz7"
#ARG git_repo_path="agepoly/it/dev/dameblanche"
#ARG git_temp_path="/tmp/dameblanche"
#ARG pip_reqs=""
#ARG www_root_path="/var/www/html"
#
#RUN git clone  --progress --verbose --single-branch  --branch $git_branch https://$gitlab_token_name:$gitlab_token_password@gitlab.com/$git_repo_path $git_temp_path
#RUN cp -r $git_temp_path/dameblanche/* -t $www_root_path/.
#RUN rm -fr $git_temp_path
##

## If you dev in local uncomment this block
COPY dameblanche/ /var/www/html
RUN chown -R www-data: /var/www/
##

RUN a2dissite 000-default 
COPY dameblanche.apache /etc/apache2/sites-available/$project_name.conf
RUN a2ensite $project_name

CMD ["apachectl", "-D", "FOREGROUND"]

EXPOSE 80
