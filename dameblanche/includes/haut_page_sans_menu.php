<?php
// On se connecte � la base de donn�es
include('./includes/identifiants.php');
mysql_connect($adresse, $nom, $motdepasse);
mysql_select_db($database);

// choix de la langue
$lang=mysql_real_escape_string($_GET['lang']);
$lang_cookie=stripslashes(htmlspecialchars($_COOKIE['lang']));
$expire=time()+365*24*3600;
if ($lang=='en' || $lang=='fr') {
	if ($lang!=$lang_cookie) {setcookie('lang', $lang, $expire);}
} else {
	if ($lang_cookie=='en' || $lang_cookie=='fr') {
		$lang=$lang_cookie;
	} else {
		$lang='fr';
		setcookie('lang', $lang, $expire);
	}
}
// titre de la page dans la bonne langue
if ($lang=='en') {$titre=$titre_de;}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
	<head>
		<!-- Titre de la page -->
		<title><?php echo ''.$titre.''; ?></title>
		<meta name="keywords" content=""/>
		<meta name="author" content="Lugon-Moulin Dionys"/>
		<meta name="description" content=""/>
		<meta name="category" content=""/>
		<link rel="stylesheet" type="text/css" media="print" href="./print.css"/>
		<link rel="stylesheet" media="screen" type="text/css" title="Design" href="./design.css"/>

		<!-- Le favicon doit faire 16x16 px et 256 couleurs max. G�n�rer un favicon : http://www.chami.com/html-kit/services/favicon/ et http://www.favicon.cc/ -->
		<link rel="icon" type="image/gif" href="./images/favicon.ico"/>
		<!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="./images/favicon.ico" /><![endif]-->
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>

		<!-- Javascript pour le menu -->
		<script type="text/javascript" src="./includes/menu.js"></script>
		<!-- JS pour le CKEditor -->
		<script type="text/javascript" src="./ckeditor/ckeditor.js"></script>
		<!-- Google Analytics -->

	</head>

<!-- On commence notre page (le contenu) -->
<body>

<!-- On affiche le conteneur, le haut du design, le menu et le calendrier -->
<div id="conteneur">
	<div id="top"></div>
	<div id="haut">
		<!-- <div id="header"><img src="<?php echo $banniere; ?>" height="210px" width="780px" alt="banniere"/></div> -->
		<div id="header"><a href="./index.php"><img src="./images/banniere_dameblanche.png" alt="banniere"/></a></div>
		<div id="calendrier"><?php include('./includes/calendrier.php')?></div>
		<?php
		echo '<div id="milieu">';
		echo '<div id="menu">';
		// bug avec les cookies de connexion ... include('./includes/menu.php');
		echo '</div>';
		if ($lang=='en') {
			echo '<p id="lang"><a href="'.$redir.'lang=fr">French</a><br /><a href="'.$redir.'lang=en">English</a></p>';
		} else {
			echo '<p id="lang"><a href="'.$redir.'lang=fr">Fran�ais</a><br /><a href="'.$redir.'lang=en">Anglais</a></p>';
		}
		echo '</div>';
		?>
	</div>
	
	<!-- Ici commence le contenu de notre page -->
	<div id="corps">