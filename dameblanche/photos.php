<?php
// On debute le chronometre qui va calculer le temps d'execution de la page
$calcul = microtime(true);

// Pour les membres connectes
session_start();

// On donne une valeur a certaines variables
$titre = 'Les Photos';
$rafraichir = false;
$rafraichir_lien = '';

include('./includes/haut_page.php');

// On se connecte a la base de donnees
include('./includes/identifiants.php');
mysql_connect($adresse, $nom, $motdepasse);
mysql_select_db($database);

// Membre connecte
if (isset($_SESSION['id']))
{
	$action = htmlspecialchars($_GET['action']);
	
	switch($action)
	{
	case "voir" :
		$photo_id = (intval($_GET['id'])-1);
		$photo_cat = mysql_real_escape_string($_GET['cat']);
		$recherche_photo = mysql_query('SELECT photo_id, photo_titre, photo_description, photo_categorie, photo_lien FROM photos WHERE photo_categorie = "'.$photo_cat.'" LIMIT '.$photo_id.', 1')or die(mysql_error());
		$photo = mysql_fetch_array($recherche_photo);
		$total_photo = mysql_query('SELECT photo_categorie FROM photos WHERE photo_categorie = "'.$photo_cat.'" ')or die(mysql_error());
		$total_cat = (mysql_num_rows($total_photo)-1);
		if ($photo_id == 0)
		{
			if ($total_cat != 0)
			{
			echo '<a href="./photos.php">Retourner aux albums photo</a> | <a href="./photos.php?action=voir&cat='.stripslashes(htmlspecialchars($photo['photo_categorie'])).'&id='.($photo_id+2).'">suivante >>></a><br />';
			}
			else
			{
			echo '<a href="./photos.php">Retourner aux albums photo.</a><br />';
			}
		}
		elseif ($photo_id == $total_cat)
		{
			echo '<a href="./photos.php?action=voir&cat='.stripslashes(htmlspecialchars($photo['photo_categorie'])).'&id='.($photo_id).'"><<< precedente</a> | <a href="./photos.php">Retourner aux albums photo</a><br />';
		}
		else
		{
			echo '<a href="./photos.php?action=voir&cat='.stripslashes(htmlspecialchars($photo['photo_categorie'])).'&id='.($photo_id).'"><<< precedente</a> | <a href="./photos.php">Retourner aux albums photo</a> | ';
			echo '<a href="./photos.php?action=voir&cat='.stripslashes(htmlspecialchars($photo['photo_categorie'])).'&id='.($photo_id+2).'">suivante >>></a><br />';
		}
		echo '<h1>'.stripslashes(htmlspecialchars($photo['photo_titre'])).'</h1>';
		echo '<img src="./images/photos/redim/'.stripslashes(htmlspecialchars($photo['photo_lien'])).'" alt="Photo"/><br />';
		echo '<p>Description :<br />'.$photo['photo_description'].'</p>';
		$admin_photo = mysql_query('SELECT membre_id, admin_photo FROM membres WHERE membre_id = "'.$_SESSION['id'].'"')or die(mysql_error());
		$admin = mysql_fetch_array($admin_photo);
		if ($admin['admin_photo'] == 'ok')
		{
		echo '<a href="./photosok.php?action=edit&id='.intval($photo['photo_id']).'">Editer</a> ou <a href="./photosok.php?action=del&id='.intval($photo['photo_id']).'">Supprimer</a> la photo.';
		}
	break;
	
	case "ajout" :
		$admin_photo = mysql_query('SELECT membre_id, admin_photo FROM membres WHERE membre_id = "'.$_SESSION['id'].'"')or die(mysql_error());
		$admin = mysql_fetch_array($admin_photo);
		if ($admin['admin_photo'] == 'ok')
		{
		echo '<h1>Administration des photos</h1>';
		echo '<p>Attention ! Ajouter une photo sur le site peut prendre du temps surtout si elle est lourde. Patientez jusqu\'a ce que la page se charge.</p>';
		echo '<form class="photo" method="post" action="./photosok.php" enctype="multipart/form-data">';
		echo '<fieldset><legend>Ajouter une photo</legend>';
		echo '<label for="photo">Votre photo : </label><input type="file" name="photo" id="photo" /><br />';
		echo '<label for="choix1">Choississez la categorie : </label>';
		echo '<select name="choix1" id="choix1">';
		$possibilites = mysql_query('SELECT DISTINCT photo_categorie FROM photos')or die(mysql_error());
		while ($choix = mysql_fetch_array($possibilites))
		{
		echo '<option value="'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'">'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'</option>';
		}
		echo '</select>';
		echo '<label for="choix2"> ou nommez-en une nouvelle : </label><input type="text" name="choix2" id="choix2" /><br />';
		echo '<label for="titre">Titre de la photo : </label><input type="text" name="titre" id="titre" /><br />';
		echo '<label for="description">Description de la photo : </label><br /><textarea name="description" id="description"></textarea>';
		echo '<input type="hidden" name="action" value="ajout" />';
		echo '</fieldset>';
		echo '<input type="submit" name="submit" value="Envoyer la photo" />';
		echo '</form>';
		echo '<p><a href="'.$page_precedente.'">Revenir a la page precedente</a></p>';
		}
		else
		{
		echo '<p>Vous n\'avez pas le droit d\'administrer les photos.<br /><br />Revenir a la <a href="'.$page_precedente.'">page precedente</a></p>';
		}
	break;
	
	case "menu" :
		?>
		<h1>Administration des Photos</h1>
		<h3>Ajouter une Photo</h3>
		<p><a href="./photos.php?action=ajout">Ajouter une Photo</a></p><br />
		<h3>Editer/Supprimer une Photo</h3>
		<?php
		$demande1 = mysql_query('SELECT * FROM photos')or die(mysql_error());
		$nbre_news = mysql_num_rows($demande1);
		if($nbre_news != 0)
		{
			$news_page = 20;
			$pages = ceil($nbre_news / $news_page);
			echo 'Page : ';
			for ($i = 1 ; $i <= $pages ; $i++)
			{
				if ($i == $pages) // On ne met pas de lien sur la page actuelle
				{
				echo ''.$i. '';
				}
				else
				{
				echo '<a href="./photos.php?page='.$i.'">'.$i.'</a>';
				}
			}
			echo '<br /><br />';
			if (isset($_GET['page'])) {$page = intval($_GET['page']);}
			else {$page = 1;}
			$news_affiche = ($page - 1) * $news_page;
			$demande2 = mysql_query('SELECT photo_id, photo_titre, photo_categorie, photo_date FROM photos ORDER BY photo_categorie DESC LIMIT '.$news_affiche.', '.$news_page.' ')or die(mysql_error());
			while($resultat2 = mysql_fetch_array($demande2))
			{
				echo ''.date('j / n / Y', $resultat2['photo_date']).' - '.stripslashes(htmlspecialchars($resultat2['photo_categorie'])).' -
				<strong>'.stripslashes(htmlspecialchars($resultat2['photo_titre'])).'</strong> - 
				<a href="./photosok.php?action=edit&amp;id='.intval($resultat2['photo_id']).'">Editer</a> ou
				<a href="./photosok.php?action=del&amp;id='.intval($resultat2['photo_id']).'">supprimer</a> la Photo.<br /><br />';
			}
			echo 'Page : ';
			for ($i = 1 ; $i <= $pages ; $i++)
			{
				if ($i == $pages) // On ne met pas de lien sur la page actuelle
				{
				echo ''.$i. '';
				}
				else
				{
				echo '<a href="./photos.php?page='.$i.'">'.$i.'</a>';
				}
			}
		}
		else
		{
		echo 'Il n\'y a pas de photos dans la base de donnee : <a href="./photos.php?action=ajout">Ajouter une Photo</a><br />';
		}
	break;
	
	default :
	echo '<h1>Les albums photo</h1>';
	$possibilites = mysql_query('SELECT DISTINCT photo_categorie FROM photos ORDER BY photo_date DESC')or die(mysql_error());
	if (mysql_num_rows($possibilites) != 0)
	{
		while ($choix = mysql_fetch_array($possibilites))
		{
			$i = 1;
			$j = 1;
			echo '<h3>'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'</h3>';
			$photo = mysql_query('SELECT photo_id, photo_categorie, photo_lien FROM photos WHERE photo_categorie = "'.$choix['photo_categorie'].'" ')or die(mysql_error());
			echo '<table id="album_photo">';
			while ($miniature = mysql_fetch_array($photo))
			{
				if ($j == 1) {echo '<tr>';} else{;}
				echo '<td><a href="./photos.php?action=voir&cat='.$choix['photo_categorie'].'&id='.$i.'"><img src="./images/photos/miniatures/'.$miniature['photo_lien'].'" alt="Photo"/></a></td>';
				if ($j == 3) {echo '</tr>'; $j = 0;} else{;}
				$i++;
				$j++;
			}
			echo '</table>';
		}
	}
	else
	{
	echo '<p>Il n\'y a pas de photos actuellement.</p>';
	echo '<p>Cliquez <a href="./index.php">ici</a> pour revenir a la page d\'accueil.</p>';
	echo '<p>Revenir a la <a href="'.$page_precedente.'">page precedente</a></p>';
	}
	}
}
else
{
echo '<p>Vous n\'etes pas connecte.<br />Vous devez vous inscrire sur le site pour pouvoir voir les photos des membres de l\'amicale.<br /><br />Revenir a la <a href="'.$page_precedente.'">page precedente</a></p>';
}

include('./includes/bas_page.php'); ?>