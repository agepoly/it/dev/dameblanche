<?php

// Inclus dans la page admin.php
// variables $choix et $id

switch($action) {
	case "new" :
		$i = 0;
		echo '<h3>Ajouter une Photo</h3>';
		if (mysql_real_escape_string($_POST['confirm']=='ok')) {
			$photo = mysql_real_escape_string($_POST['photo']);
			if ($_POST['choix2'] == '') {$cat_choix = mysql_real_escape_string($_POST['choix1']);}
			else {$cat_choix = mysql_real_escape_string($_POST['choix2']);}
			$titre = mysql_real_escape_string($_POST['titre']);
			// + en allemand !!!
			
			/* Ce bloc n�cessaire ?
			// V�rification du titre
			if (strlen($titre) > 50) {
				$erreur .= '<li>Votre titre est trop long, il doit faire moins de 50 caract�res.</li>';
				$i++;
			}
			if ($_POST['titre'] == '') {
				$erreur .= '<li>Vous n\'avez pas saisi de titre.</li>';
				$i++;
			}
			// V�rification de la cat�gorie
			if (strlen($cat_choix) > 200) {
				$erreur .= '<li>Votre cat�gorie est trop longue, elle doit faire moins de 200 caract�res.</li>';
				$i++;
			}
			*/
			// V�rification de la photo
			if (!empty($_FILES['photo']['size'])) {
				// On d�finit les variables :
				$maxsize = 10240000;
				// Liste des extensions valides
				$extensions_valides = array('jpg', 'jpeg', 'gif', 'png');
			 
				if ($_FILES['photo']['error'] > 0) {
					$erreur .= '<li>Erreur lors du tranfsert de la photo.</li>';
					$i++;
				}
				if ($_FILES['photo']['size'] > $maxsize) {
					$erreur .= '<li>Le fichier est trop gros : (<strong>'.$_FILES['photo']['size'].' Octets</strong> contre <strong>'.$maxsize.' Octets autoris�s</strong>)</li>';
					$i++;
				}
				$extension_upload = strtolower(substr(strrchr($_FILES['photo']['name'], '.'), 1));
				if (!in_array($extension_upload,$extensions_valides) ) {
					$erreur .= '<li>Extension de la photo incorrecte : (Vous devez utiliser : .jpg, .jpeg, .gif, .png)</li>';
					$i++;
				}
			} else {
				$erreur .= '<li>La photo est manquante, veuillez en ajouter une autre.</li>';
				$i++;
			}
			
			if ($i != 0) {
				echo '<p>Ajouter une photo sur le site peut prendre du temps surtout si elle est lourde. Patientez jusqu\'� ce que la page se charge.</p>';
				echo '<p><ul>'.$erreur.'</ul></p>';
				echo '<form class="photo" method="post" action="./admin.php?cat=photos" enctype="multipart/form-data">';
				echo '<fieldset><legend>Ajouter une photo</legend>';
				echo '<label for="photo">Votre photo : </label><input type="file" name="photo" id="photo" /><br />';
				echo '<label for="choix1">Choisissez la cat�gorie : </label>';
				echo '<select name="choix1" id="choix1">';
				$possibilites = mysql_query('SELECT DISTINCT photo_categorie FROM photos')or die(mysql_error());
				while ($choix = mysql_fetch_array($possibilites))
				{
				echo '<option value="'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'">'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'</option>';
				}
				// + la faire s�lectionn�e automatiquement si d�j� choisit... et si aucune cat�gorie... enlever le choix
				echo '</select>';
				echo '<label for="choix2"> ou nommez-en une nouvelle : </label><input type="text" name="choix2" id="choix2" value="'.$_POST['choix2'].'" /><br />';
				echo '<label for="titre">Titre de la photo : </label><input type="text" name="titre" id="titre" value="'.$_POST['titre'].'" /><br />';
				echo '<input type="hidden" name="action" value="ajout" />';
				echo '</fieldset>';
				echo '<input type="submit" name="submit" value="Envoyer la photo" />';
				echo '</form>';
				echo '<p><a href="./admin.php?cat=photos">Retour</a></p>';
			} else {
				// On d�place la photo
				$nom_photo = $_FILES['photo']['name'];
				$photo_dossier = './images/photos/'.$nom_photo.'';
				if(move_uploaded_file($_FILES['photo']['tmp_name'],$photo_dossier)) {
					mysql_query('INSERT INTO photos (photo_titre, photo_categorie, photo_date, photo_lien)
					VALUES ("'.$titre.'", "'.$cat_choix.'", "'.time().'", "'.$nom_photo.'")')or die(mysql_error());
					
					// Variables
					$mini_dim_x=200;
					$mini_dim_y=150;
					$redim_dim_y=680;
					
					// Cr�ation des miniatures
					if ($extension_upload == 'jpg' || $extension_upload == 'jpeg') {$source = imagecreatefromjpeg('./images/photos/'.$nom_photo.'');}
					elseif ($extension_upload == 'gif') {$source = imagecreatefromgif('./images/photos/'.$nom_photo.'');}
					elseif ($extension_upload == 'png') {$source = imagecreatefrompng('./images/photos/'.$nom_photo.'');}
					$largeur_source = imagesx($source);
					$hauteur_source = imagesy($source);
					$facteur_dim_x = ($mini_dim_x/$largeur_source);
					$facteur_dim_y = ($mini_dim_y/$hauteur_source);
					if ($facteur_dim_x > $facteur_dim_y) {$facteur_dim = $facteur_dim_y;}
					else {$facteur_dim = $facteur_dim_x;}
					$destination = imagecreatetruecolor(floor($largeur_source*$facteur_dim), floor($hauteur_source*$facteur_dim)); // On cr�e la miniature vide
					$largeur_destination = imagesx($destination);
					$hauteur_destination = imagesy($destination);
					// On g�n�re enfin la miniature
					imagecopyresampled($destination, $source, 0, 0, 0, 0, $largeur_destination, $hauteur_destination, $largeur_source, $hauteur_source);
					if ($extension_upload == 'jpg' || $extension_upload == 'jpeg') {imagejpeg($destination, 'images/photos/miniatures/'.$nom_photo, 100);}
					elseif ($extension_upload == 'gif') {imagegif($destination, 'images/photos/miniatures/'.$nom_photo, 100);}
					elseif ($extension_upload == 'png') {imagepng($destination, 'images/photos/miniatures/'.$nom_photo, 100);}
					
					// Cr�ation des photos redimensionn�es � afficher
					$facteur_dim2 = ($redim_dim_y/$largeur_source);
					$destination2 = imagecreatetruecolor(floor($largeur_source*$facteur_dim2), floor($hauteur_source*$facteur_dim2)); // On cr�e la photo vide
					$largeur_destination2 = imagesx($destination2);
					$hauteur_destination2 = imagesy($destination2);
					// On g�n�re la photo
					imagecopyresampled($destination2, $source, 0, 0, 0, 0, $largeur_destination2, $hauteur_destination2, $largeur_source, $hauteur_source);
					if ($extension_upload == 'jpg' || $extension_upload == 'jpeg') {imagejpeg($destination2, 'images/photos/redim/'.$nom_photo, 100);}
					elseif ($extension_upload == 'gif') {imagegif($destination2, 'images/photos/redim/'.$nom_photo, 100);}
					elseif ($extension_upload == 'png') {imagepng($destination2, 'images/photos/redim/'.$nom_photo, 100);}
					
					echo '<p>Photo ajout�e</p>';
					echo '<p><a href="./admin.php?cat=photos">Retour</a></p>';
				} else {
					echo '<h1>Echec de l\'upload</h1>';
					echo '<p><a href="./admin.php?cat=photos">Retour</a></p>';
				}
			}
		} else {
			echo '<p>Ajouter une photo sur le site peut prendre du temps surtout si elle est lourde. Patientez jusqu\'� ce que la page se charge.</p>';
			echo '<form class="photo" method="post" action="./admin.php?cat=photos" enctype="multipart/form-data">';
			echo '<fieldset><legend>Ajouter une photo</legend>';
			echo '<label for="photo">Votre photo : </label><input type="file" name="photo" id="photo" /><br />';
			echo '<label for="choix1">Choisissez la cat�gorie : </label>';
			echo '<select name="choix1" id="choix1">';
			$possibilites = mysql_query('SELECT DISTINCT photo_categorie FROM photos')or die(mysql_error());
			while ($choix = mysql_fetch_array($possibilites))
			{
			echo '<option value="'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'">'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'</option>';
			}
			// + la faire s�lectionn�e automatiquement si d�j� choisit... et si aucune cat�gorie... enlever le choix
			echo '</select>';
			echo '<label for="choix2"> ou nommez-en une nouvelle : </label><input type="text" name="choix2" id="choix2" /><br />';
			echo '<label for="titre">Titre de la photo : </label><input type="text" name="titre" id="titre" /><br />';
			echo '<input type="hidden" name="action" value="ajout" />';
			echo '</fieldset>';
			echo '<input type="submit" name="submit" value="Envoyer la photo" />';
			// + le champ cach� confirm=ok
			echo '</form>';
			echo '<p><a href="./admin.php?cat=photos">Retour</a></p>';
		}
	break;
	
	case "edit" :
		$infos_photo = mysql_query('SELECT photo_id, photo_titre, photo_description, photo_categorie, photo_lien FROM photos WHERE photo_id = "'.$photo_id.'"')or die(mysql_error());
		$infos = mysql_fetch_array($infos_photo);
		echo '<h1>Editer une photo</h1>';
		echo '<form class="photo" method="post" action="./admin.php?cat=photos" enctype="multipart/form-data">';
		echo '<fieldset><legend>Editer une photo</legend>';
		echo '<img src="./images/photos/miniatures/'.$infos['photo_lien'].'" alt="Photo"/><br />';
		echo '<label for="choix1">Choississez la cat�gorie : </label>';
		echo '<select name="choix1" id="choix1">';
		$possibilites = mysql_query('SELECT DISTINCT photo_categorie FROM photos')or die(mysql_error());
		while ($choix = mysql_fetch_array($possibilites))
		{
			if ($infos['photo_categorie'] == $choix['photo_categorie']) {$selection = 'selected="selected"';} else {$selection = '';}
			echo '<option value="'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'" '.$selection.'>'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'</option>';
		}
		echo '</select>';
		echo '<label for="choix2"> ou nommez-en une nouvelle : </label><input type="text" name="choix2" id="choix2"/><br />';
		echo '<label for="titre">Titre de la photo : </label><input type="text" name="titre" id="titre" value="'.stripslashes($infos['photo_titre']).'" /><br />';
		echo '<label for="description">Description de la photo : </label><br /><textarea name="description" id="description">'.stripslashes($infos['photo_description']).'</textarea>';
		echo '<input type="hidden" name="action" value="editok" />';
		echo '<input type="hidden" name="id" value="'.$photo_id.'" />';
		echo '</fieldset>';
		echo '<input type="submit" name="submit" value="Editer les infos" />';
		echo '</form>';
		echo '<p>Cliquez <a href="./index.php">ici</a> pour revenir � la page d\'accueil.</p>';
		echo '<p>Cliquez <a href="./photos.php?action=ajout">ici</a> pour ajouter une nouvelle photo.</p>';
		break;
		
		case "editok" :
		$i = 0;
		if ($_POST['choix2'] == '') {$cat_choix = mysql_real_escape_string($_POST['choix1']);}
		else {$cat_choix = mysql_real_escape_string($_POST['choix2']);}
		$titre = mysql_real_escape_string($_POST['titre']);
		$description = mysql_real_escape_string($_POST['description']);
		
		// On v�rifie le titre
		if (strlen($titre) > 50)
		{
			$erreur .= '<li>Votre titre est trop long, il doit faire moins de 50 caract�res.</li>';
			$i++;
		}
		if ($_POST['titre'] == '')
		{
			$erreur .= '<li>Vous n\'avez pas saisi de titre.</li>';
			$i++;
		}
		// On v�rifie la cat�gorie
		if (strlen($cat_choix) > 200)
		{
			$erreur .= '<li>Votre cat�gorie est trop longue, elle doit faire moins de 200 caract�res.</li>';
			$i++;
		}
		// On v�rifie la description
		if (strlen($description) > 500)
		{
			$erreur .= '<li>Votre description est trop longue, il doit faire moins de 500 caract�res.</li>';
			$i++;
		}
		if ($_POST['description'] == '')
		{
			$erreur .= '<li>Vous n\'avez pas saisi de description.</li>';
			$i++;
		}
		
		if ($i != 0)
		{
			echo '<h1>Editer une photo</h1>';
			echo '<p><ul>'.$erreur.'</ul></p>';
			$infos_photo = mysql_query('SELECT photo_id, photo_lien FROM photos WHERE photo_id = "'.$photo_id.'"')or die(mysql_error());
			$infos = mysql_fetch_array($infos_photo);
			echo '<form class="photo" method="post" action="./admin.php?cat=photos" enctype="multipart/form-data">';
			echo '<fieldset><legend>Editer une photo</legend>';
			echo '<img src="./images/photos/miniatures/'.$infos['photo_lien'].'" alt="Photo"/><br />';
			echo '<label for="choix1">Choississez la cat�gorie : </label>';
			echo '<select name="choix1" id="choix1">';
			$possibilites = mysql_query('SELECT DISTINCT photo_categorie FROM photos')or die(mysql_error());
			while ($choix = mysql_fetch_array($possibilites))
			{
				if ($_POST['choix1'] == $choix['photo_categorie']) {$selection = 'selected="selected"';} else {$selection = '';}
				echo '<option value="'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'" '.$selection.'>'.stripslashes(htmlspecialchars($choix['photo_categorie'])).'</option>';
			}
			echo '</select>';
			if (isset($_POST['choix2'])) {$value = 'value="'.stripslashes($_POST['choix2']).'"';}
			echo '<label for="choix2"> ou nommez-en une nouvelle : </label><input type="text" name="choix2" id="choix2" '.$value.'/><br />';
			echo '<label for="titre">Titre de la photo : </label><input type="text" name="titre" id="titre" value="'.stripslashes($_POST['titre']).'" /><br />';
			echo '<label for="description">Description de la photo : </label><br /><textarea name="description" id="description">'.stripslashes($_POST['description']).'</textarea>';
			echo '<input type="hidden" name="action" value="editok" />';
			echo '<input type="hidden" name="id" value="'.$photo_id.'" />';
			echo '</fieldset>';
			echo '<input type="submit" name="submit" value="Editer les infos" />';
			echo '</form>';
			echo '<p>Cliquez <a href="./index.php">ici</a> pour revenir � la page d\'accueil.</p>';
			echo '<p>Cliquez <a href="./photos.php?action=ajout">ici</a> pour ajouter une nouvelle photo.</p>';
		}
		else
		{
			mysql_query('UPDATE photos SET photo_titre = "'.$titre.'", photo_description = "'.$description.'", photo_categorie = "'.$cat_choix.'"
			WHERE photo_id = "'.$photo_id.'"')or die(mysql_error());
			echo '<h1>Les infos ont �t� correctement �dit�es.</h1>';
			echo '<p>Cliquez <a href="./index.php">ici</a> pour revenir � la page d\'accueil.</p>';
			echo '<p>Cliquez <a href="./photos.php?action=ajout">ici</a> pour ajouter une nouvelle photo.</p>';
		}
	break;
	
	case "del" :
		$confirm = intval($_GET['confirm']);
		$photo_id = intval($_GET['id']);
		if ($confirm == 1)
		{
			$nom_photo = mysql_query('SELECT photo_id, photo_lien FROM photos WHERE photo_id = "'.$photo_id.'"')or die(mysql_error());
			$nom = mysql_fetch_array($nom_photo);
			// Supprime l'image de base
			$dossier_traite1 = './images/photos/';
			$repertoire1 = opendir($dossier_traite1); //on d�finit le r�pertoire dans lequel on souhaite travailler
			if (false !== ($fichier1 = readdir($repertoire1))) //on lit chaque fichier du r�pertoire dans la boucle
			{
				//on met le chemin du fichier dans une variable simple
				$chemin1 = ''.$dossier_traite1.$nom['photo_lien'].'';
				//si le fichier n'est pas un r�pertoire
				if ($fichier1 != ".." AND $fichier1 != "." AND !is_dir($fichier1))
					{
						unlink($chemin1);
					}
			}
			closedir($repertoire1); //Ne pas oublier de fermer le dossier !EN DEHORS de la boucle ! Ce qui �vitera � PHP bcp de calculs et des pbs li�s � l'ouverture du dossier
			// Supprime la miniature
			$dossier_traite2 = './images/photos/miniatures/';
			$repertoire2 = opendir($dossier_traite2);
			if (false !== ($fichier2 = readdir($repertoire2)))
			{
				$chemin2 = ''.$dossier_traite2.$nom['photo_lien'].'';
				if ($fichier2 != ".." AND $fichier2 != "." AND !is_dir($fichier2))
					{
						unlink($chemin2);
					}
			}
			closedir($repertoire2);
			// Supprime la redimension
			$dossier_traite3 = './images/photos/redim/';
			$repertoire3 = opendir($dossier_traite3);
			if (false !== ($fichier3 = readdir($repertoire3)))
			{
				$chemin3 = ''.$dossier_traite3.$nom['photo_lien'].'';
				if ($fichier3 != ".." AND $fichier3 != "." AND !is_dir($fichier3))
					{
						unlink($chemin3);
					}
			}
			closedir($repertoire3);
			mysql_query('DELETE FROM photos WHERE photo_id = "'.$photo_id.'"')or die(mysql_error());
			echo '<p>Vous avez bien supprim� la photo.</p>';
			echo '<p>Cliquez <a href="./index.php">ici</a> pour revenir � la page d\'accueil.</p>';
			echo '<p>Cliquez <a href="./photos.php">ici</a> pour voir les photos.</p>';
		}
		else
		{
			echo '<h1>Supprimer une photo</h1>';
			echo 'Etes-vous s�r de vouloir supprimer cette photo ? <a href="./admin.php?cat=photos?action=del&id='.$photo_id.'&confirm=1">Oui</a> ou <a href="'.$page_precedente.'">Non</a><br /><br />';
			$demande_photo = mysql_query('SELECT photo_id, photo_lien FROM photos WHERE photo_id = "'.$photo_id.'"')or die(mysql_error());
			$resultat_photo = mysql_fetch_array($demande_photo);
			echo '<img src="./images/photos/redim/'.$resultat_photo['photo_lien'].'" alt="Photo"/><br />';
		}
	break;
	
	default:
	echo '<h1>Administration des Photos</h1>';
	echo '<h3>Ajouter une Photo</h3>';
	echo '<a href="./admin.php?cat=photo&choix=new">Ajouter une Photo</a>';
	echo '<h3>Editer/Supprimer une Photo</h3>';
	// $requete = mysql_query('SELECT titre, titre_de, memo_date FROM calendrier ORDER BY memo_date DESC') or die(mysql_error());
	// $i=0;
	// while ($data = mysql_fetch_array($requete)) {
		// echo '<p>'.stripslashes(htmlspecialchars($data['titre'])).' <=> '.stripslashes(htmlspecialchars($data['titre_de'])).'<br />';
		// echo '<a href="./admin.php?cat=calendrier&choix=edit&id='.$data['memo_date'].'">Editer</a> ';
		// echo '<a href="./admin.php?cat=calendrier&choix=del&id='.$data['memo_date'].'">Supprimer</a></p>';
		// $i++;
	// }
	if ($i==0) {echo'<p>Aucune Photo t�l�charg�e.</p>';}
	echo '<a href="./admin.php">Retour</a>';
}


?>