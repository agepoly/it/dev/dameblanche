<?php
// On d�bute le chronom�tre qui va calculer le temps d'ex�cution de la page
$timer = microtime(true);

// On donne une valeur � certaines variables
$titre = 'Administration';
$titre_de = 'Administration';
$redir = './admin.php?'; // pour le changement de langue

include('./includes/haut_page_sans_menu.php');

// On se connecte � la base de donn�es
// include('./includes/identifiants.php');
// mysql_connect($adresse, $nom, $motdepasse);
// mysql_select_db($database);

	?>
	<div id="menu_gauche">
		<p><!--Pas inclus le menu_gauche car bug...--></p>
	</div>
	<div id="contenu">
	<?php

	// On commence la page
	
	if (mysql_real_escape_string($_GET['alive'])=='deco') {
	setcookie('token', '0', time());
	$deco='true';
	} else {
	$deco='false'; }
	// $ip=$_SERVER['REMOTE_ADDR']; // chopper l'ip avec la variable server
	$ip=$_SERVER['HTTP_X_FORWARDED_FOR']; // chopper l'ip avec la variable server
	$requete = mysql_query('SELECT token, ip, time FROM session WHERE token="'.stripslashes(htmlspecialchars($_COOKIE['token'])).'"') or die(mysql_error());
	$data = mysql_fetch_array($requete);
	$ip_bdd=$data['ip'];
	$time_bdd=$data['time'];
	if ($ip==$ip_bdd and time()-$time_bdd<0 and $deco=='false') { // ok l'utilisateur a le droit d'acc�s
		echo '<p><a href="./admin.php?alive=deco">fermer la session</a></p>';
		echo '<h1>Administration du site Internet</h1>';
		// Nos 3 variables
		$cat=mysql_real_escape_string($_GET['cat']);
		$choix=mysql_real_escape_string($_GET['choix']);
		$id=mysql_real_escape_string($_GET['id']); // on prend le timestamp consid�r� comme unique
		switch($cat) {
			// ==========================================
			// Administration des Nouvelles
			// ==========================================
			case 'news':
			switch($choix){
				case 'publier':
					echo '<h3>Publier une Nouvelle</h3>';
					echo '<p>Vous avez publi� la Nouvelle.</p>';
					mysql_query('UPDATE news SET public="1", time_public="'.time().'" WHERE time="'.$id.'"')or die(mysql_error());
					echo '<a href="./admin.php?cat=news">Retour</a>';
				break;
				case 'depublier':
					echo '<h3>D�publier une Nouvelle</h3>';
					echo '<p>Vous avez depubli� la Nouvelle.</p>';
					mysql_query('UPDATE news SET public="0" WHERE time="'.$id.'"')or die(mysql_error());
					echo '<a href="./admin.php?cat=news">Retour</a>';
				break;
				case 'new':
					echo '<h3>Ajouter une Nouvelle</h3>';
					if (mysql_real_escape_string($_POST['confirm']=='ok')) {
						$titre=stripslashes(htmlspecialchars($_POST['titre']));
						$text=$_POST['editor'];
						$titre_de=stripslashes(htmlspecialchars($_POST['titre_de']));
						$text_de=$_POST['editor_de'];
						if (mb_strlen($titre)<5 or mb_strlen($titre_de)<5) {
							echo '<p>Le titre est trop court.</p>';
							echo '<form method="post" action="./admin.php?cat=news&choix=new" enctype="multipart/form-data"><p>';
							echo '<p><label for="titre">Titre de la Nouvelle en Fran�ais : </label><input type="text" name="titre" id="titre" value="'.$titre.'"/></p>';
							echo '<textarea name="editor" id="editor">'.$text.'</textarea>';
							include('./includes/config_toolbar.txt');
							echo '<p><label for="titre_de">Titre de la Nouvelle en Allemand : </label><input type="text" name="titre_de" id="titre_de" value="'.$titre_de.'"/></p>';
							echo '<textarea name="editor_de" id="editor_de">'.$text_de.'</textarea>';
							include('./includes/config_toolbar_de.txt');
							echo '<br /><input type="hidden" name="confirm" value="ok" />';
							echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
						} else {
						mysql_query('INSERT INTO news VALUES ("'.mysql_real_escape_string($titre).'", "'.mysql_real_escape_string($text).'", "'.mysql_real_escape_string($titre_de).'", "'.mysql_real_escape_string($text_de).'", "'.time().'", "0", "0", "0")');
						echo '<p>La nouvelle a bien �t� enregistr�e.</p>';
						}
					} else {
						echo '<form method="post" action="./admin.php?cat=news&choix=new" enctype="multipart/form-data"><p>';
						// nouvelle en fran�ais
						echo '<p><label for="titre">Titre de la Nouvelle en Fran�ais : </label><input type="text" name="titre" id="titre" /></p>';
						echo '<textarea name="editor" id="editor"></textarea>';
						include('./includes/config_toolbar.txt');
						// nouvelle en allemand
						echo '<p><label for="titre_de">Titre de la Nouvelle en Allemand : </label><input type="text" name="titre_de" id="titre_de" /></p>';
						echo '<textarea name="editor_de" id="editor_de"></textarea>';
						include('./includes/config_toolbar_de.txt');
						echo '<br /><input type="hidden" name="confirm" value="ok" />';
						echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
					}
					echo '<a href="./admin.php?cat=news">Retour</a>';
				break;
				case 'edit':
					echo '<h3>Editer une Nouvelle</h3>';
					if (mysql_real_escape_string($_POST['confirm']=='ok')) {
						$titre=stripslashes(htmlspecialchars($_POST['titre']));
						$text=$_POST['editor'];
						$titre_de=stripslashes(htmlspecialchars($_POST['titre_de']));
						$text_de=$_POST['editor_de'];
						if (mb_strlen($titre)<5 or mb_strlen($titre_de)<5) {
							echo '<p>Le titre est trop court.</p>';
							echo '<form method="post" action="./admin.php?cat=news&choix=edit&id='.$id.'" enctype="multipart/form-data"><p>';
							echo '<p><label for="titre">Titre de la Nouvelle en Fran�ais : </label><input type="text" name="titre" id="titre" value="'.$titre.'"/></p>';
							echo '<textarea name="editor" id="editor">'.$text.'</textarea>';
							include('./includes/config_toolbar.txt');
							echo '<p><label for="titre_de">Titre de la Nouvelle en Allemand : </label><input type="text" name="titre_de" id="titre_de" value="'.$titre_de.'"/></p>';
							echo '<textarea name="editor_de" id="editor_de">'.$text_de.'</textarea>';
							include('./includes/config_toolbar_de.txt');
							echo '<br /><input type="hidden" name="confirm" value="ok" />';
							echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
						} else {
						mysql_query('UPDATE news SET titre="'.mysql_real_escape_string($titre).'", contenu="'.mysql_real_escape_string($text).'", titre_de="'.mysql_real_escape_string($titre_de).'", contenu_de="'.mysql_real_escape_string($text_de).'", time_edit="'.time().'" WHERE time='.$id.'')or die(mysql_error());
						echo '<p>La nouvelle a bien �t� enregistr�e.</p>';
						}
					} else{ 
						echo '<form method="post" action="./admin.php?cat=news&choix=edit&id='.$id.'" enctype="multipart/form-data"><p>';
						// r�cup�rer les donn�es dans la bdd
						$requete = mysql_query('SELECT titre, contenu, titre_de, contenu_de, time FROM news WHERE time='.$id.'') or die(mysql_error());
						$data = mysql_fetch_array($requete);
						// nouvelle en fran�ais
						echo '<p><label for="titre">Titre de la Nouvelle en Fran�ais : </label><input type="text" name="titre" id="titre" value="'.$data['titre'].'"/></p>';
						echo '<textarea name="editor" id="editor">'.$data['contenu'].'</textarea>';
						include('./includes/config_toolbar.txt');
						// nouvelle en allemand
						echo '<p><label for="titre_de">Titre de la Nouvelle en Allemand : </label><input type="text" name="titre_de" id="titre_de" value="'.$data['titre_de'].'"/></p>';
						echo '<textarea name="editor_de" id="editor_de">'.$data['contenu_de'].'</textarea>';
						include('./includes/config_toolbar_de.txt');
						echo '<br /><input type="hidden" name="confirm" value="ok" />';
						echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
					}
					echo '<a href="./admin.php?cat=news">Retour</a>';
				break;
				case 'del':
					echo '<h3>Supprimer une Nouvelle</h3>';
					if (mysql_real_escape_string($_GET['confirm'])=="ok") {
						mysql_query('DELETE FROM news WHERE time = "'.$id.'"')or die(mysql_error());
						echo '<p>Vous avez supprime la Nouvelle.</p>';
						} else {
						echo '<p>Etes-vous s�r de supprimer cette Nouvelle ? <a href="./admin.php?cat=news&choix=del&id='.$id.'&confirm=ok">OUI</a></p>';
						}
					echo '<a href="./admin.php?cat=news">Retour</a>';
				break;
				default:
				echo '<h1>Administration des Nouvelles</h1>';
				echo '<h3>Ajouter une Nouvelle</h3>';
				echo '<a href="./admin.php?cat=news&choix=new">Ajouter une Nouvelle</a>';
				echo '<h3>Publier/Editer/Supprimer une Nouvelle</h3>';
				$requete = mysql_query('SELECT titre, titre_de, time, public FROM news ORDER BY time DESC') or die(mysql_error());
				$i=0;
				while ($data = mysql_fetch_array($requete)) {
					if ($data['public']=="0") {$pub='publier'; $publie_txt='Publier';} else {$pub='depublier'; $publie_txt='Depublier';}
					echo '<p>'.stripslashes(htmlspecialchars($data['titre'])).' <=> '.stripslashes(htmlspecialchars($data['titre_de'])).'<br />';
					echo '<a href="./admin.php?cat=news&choix='.$pub.'&id='.$data['time'].'">'.$publie_txt.'</a> ';
					echo '<a href="./admin.php?cat=news&choix=edit&id='.$data['time'].'">Editer</a> ';
					echo '<a href="./admin.php?cat=news&choix=del&id='.$data['time'].'">Supprimer</a></p>';
					$i++;
				}
				if ($i==0) {echo'<p>Aucune Nouvelle cr��e.</p>';}
				echo '<a href="./admin.php">Retour</a>';
			}
			break;
			// ==========================================
			// Administration des Images pour les h�berger
			// ==========================================
			case 'upload':
				if (isset($_POST['action'])) {$action = mysql_real_escape_string($_POST['action']);} else {$action = mysql_real_escape_string($_GET['action']);}
				// variables � modifier
				$adresse='http://chess.epfl.ch'; // Attention aussi aux droits chmod 777 sur chaque dossier !
				$largeur_site=700;
				switch($action)
				{
				case "voir" :
					$chemin = mysql_real_escape_string($_GET['chemin']);
					echo '<h1>Url de l\'image</h1>';
					echo '<img src="./images/images/redim/'.$chemin.'" alt=""/><br />';
					echo '<p>URL de l\'image : <input type="text" id="url_img" value="'.$adresse.'/images/images/redim/'.$chemin.'"/><p>';
					echo '<p>Cliquez <a href="./admin.php">ici</a> pour revenir � la page d\'accueil.</p>';
					echo '<p>Cliquez <a href="./admin.php?cat=upload">ici</a> pour revenir � la gestion des images.</p>';
				break;
				case "ajout" :
					echo '<h1>Ajouter une image</h1>';
					echo '<p>Attention ! Ajouter une image sur le site peut prendre du temps surtout si elle est lourde. Patientez jusqu\'� ce que la page se charge.</p>';
					echo '<form class="image" method="post" action="./admin.php?cat=upload" enctype="multipart/form-data">';
					echo '<fieldset><legend>Ajouter une image</legend><div>';
					echo '<label for="largeur">Largeur de l\'image redimensionn�e : </label><input type="text" name="largeur" id="largeur" /> pixels. (0 pour aucune redimension)<br />';
					echo '<label for="image">Votre image : </label><input type="file" name="image" id="image" />';
					echo '<input type="hidden" name="action" value="ajoutok" /></div></fieldset>';
					echo '<div><br /><input type="submit" name="submit" value="Envoyer l\'image" /></div>';
					echo '</form>';
					echo '<p>Cliquez <a href="./admin.php">ici</a> pour revenir � la page d\'accueil.</p>';
					echo '<p>Cliquez <a href="./admin.php?cat=upload">ici</a> pour revenir � la gestion des images.</p>';
				break;
				case "ajoutok" :
					$i = 0;
					$image = mysql_real_escape_string($_POST['image']);
					$largeur_demandee = intval($_POST['largeur']);
					// V�rification de la largeur
					if ($largeur_demandee > $largeur_site)
					{
						$erreur .= '<li>Votre largeur est trop grande, elle doit �tre inf�rieure � '.$largeur_site.' pixels.</li>';
						$i++;
					}
					if (!isset($_POST['largeur']))
					{
						$erreur .= '<li>Il manque la largeur de l\'image.</li>';
						$i++;
					}
					// V�rification de l'image
					if (!empty($_FILES['image']['size']))
					{
						// On d�finit les variables :
						$maxsize = 10240000;
						// Liste des extensions valides
						$extensions_valides = array('jpg', 'jpeg', 'gif', 'png');
						if ($_FILES['image']['error'] > 0)
						{
							$erreur .= '<li>Erreur lors du tranfsert de l\'image.</li>';
							$i++;
						}
						if ($_FILES['image']['size'] > $maxsize)
						{
							$erreur .= '<li>Le fichier est trop gros : (<strong>'.$_FILES['image']['size'].' Octets</strong> contre <strong>'.$maxsize.' Octets autoris�s</strong>)</li>';
							$i++;
						}
						$extension_upload = strtolower(substr(strrchr($_FILES['image']['name'], '.'), 1));
						if (!in_array($extension_upload,$extensions_valides))
						{
							$erreur .= '<li>Extension de l\'image incorrecte : (Vous devez utiliser : .jpg, .jpeg, .gif, .png)</li>';
							$i++;
						}
					}
					else
					{
						$erreur .= '<li>L\'image est trop grande, elle doit peser moins de 2Mo ou l\'image est manquante, veuillez en ajouter une autre.</li>';
						$i++;
					}
					if ($i != 0)
					{
						echo '<h1>Ajouter une image</h1>';
						echo '<p>Attention ! Ajouter une image sur le site peut prendre du temps surtout si elle est lourde. Patientez jusqu\'� ce que la page se charge.</p>';
						echo '<p><ul>'.$erreur.'</ul></p>';
						echo '<form class="image" method="post" action="./admin.php?cat=upload" enctype="multipart/form-data">';
						echo '<fieldset><legend>Ajouter une image</legend><div>';
						echo '<label for="largeur">Largeur de l\'image redimensionn�e : </label><input type="text" name="largeur" id="largeur" value="'.$largeur_demandee.'" /> pixels. (0 pour aucune redimension)<br />';
						echo '<label for="image">Votre image : </label><input type="file" name="image" id="image" />';
						echo '<input type="hidden" name="action" value="ajoutok" /></div></fieldset>';
						echo '<div><br /><input type="submit" name="submit" value="Envoyer l\'image" /></div>';
						echo '</form>';
						echo '<p>Cliquez <a href="./admin.php">ici</a> pour revenir � la page d\'accueil.</p>';
						echo '<p>Cliquez <a href="./admin.php?cat=upload">ici</a> pour revenir � la gestion des images.</p>';
					}
					else
					{
						// On d�place l'image
						$extension_upload = strtolower(substr(strrchr($_FILES['image']['name'], '.'), 1));
						// $nom_image = time().'.'.$extension_upload;
						$nom_image = $_FILES['image']['name']; // pas tr�s s�curis�...
						// ATTENTION, �crase si m�me nom
						// png ne marche pas, ne cr�e pas de mini et redim...
						$photo_dossier = './images/images/'.$nom_image.'';
						if(move_uploaded_file($_FILES['image']['tmp_name'],$photo_dossier))
						{
							// On cr�e les miniatures
							if ($extension_upload == 'jpg' || $extension_upload == 'jpeg') {$source = imagecreatefromjpeg('./images/images/'.$nom_image.'');}
							if ($extension_upload == 'gif') {$source = imagecreatefromgif('./images/images/'.$nom_image.'');}
							if ($extension_upload == 'png') {$source = imagecreatefrompng('./images/images/'.$nom_image.'');}
							$largeur_source = imagesx($source);
							$hauteur_source = imagesy($source);
							$facteur_dim_x = (150/$largeur_source);
							$facteur_dim_y = (100/$hauteur_source);
							if ($facteur_dim_x > $facteur_dim_y) {$facteur_dim = $facteur_dim_y;}
							else {$facteur_dim = $facteur_dim_x;}
							$destination = imagecreatetruecolor(floor($largeur_source*$facteur_dim), floor($hauteur_source*$facteur_dim)); // On cr�e la miniature vide
							$largeur_destination = imagesx($destination);
							$hauteur_destination = imagesy($destination);
							// On g�n�re enfin la miniature
							imagecopyresampled($destination, $source, 0, 0, 0, 0, $largeur_destination, $hauteur_destination, $largeur_source, $hauteur_source);
							if ($extension_upload == 'jpg' || $extension_upload == 'jpeg') {imagejpeg($destination, 'images/images/mini/'.$nom_image, 100);}
							if ($extension_upload == 'gif') {imagegif($destination, 'images/images/mini/'.$nom_image, 100);}
							if ($extension_upload == 'png') {imagepng($destination, 'images/images/mini/'.$nom_image, 100);}
							
							// On cr�e les images redimensionn�es � afficher
							if ($largeur_demandee == 0)
							{
								if($largeur_source > $largeur_site) {$largeur_demandee = $largeur_site;} else {$largeur_demandee = $largeur_source;}
							}
							$facteur_dim2 = ($largeur_demandee/$largeur_source);
							$destination2 = imagecreatetruecolor(floor($largeur_source*$facteur_dim2), floor($hauteur_source*$facteur_dim2)); // On cr�e l'image vide
							$largeur_destination2 = imagesx($destination2);
							$hauteur_destination2 = imagesy($destination2);
							// On g�n�re l'image
							imagecopyresampled($destination2, $source, 0, 0, 0, 0, $largeur_destination2, $hauteur_destination2, $largeur_source, $hauteur_source);
							if ($extension_upload == 'jpg' || $extension_upload == 'jpeg') {imagejpeg($destination2, 'images/images/redim/'.$nom_image, 100);}
							if ($extension_upload == 'gif') {imagegif($destination2, 'images/images/redim/'.$nom_image, 100);}
							if ($extension_upload == 'png') {imagepng($destination2, 'images/images/redim/'.$nom_image, 100);}
							echo '<h1>Image ajout�e</h1>';
							echo '<img src="./images/images/redim/'.$nom_image.'" alt=""/><br />';
							echo '<p>URL de l\'image : <input type="text" id="url_img" value="'.$adresse.'/images/images/redim/'.$nom_image.'"/><p>';
							echo '<p>Cliquez <a href="./admin.php">ici</a> pour revenir � la page d\'accueil.</p>';
							echo '<p>Cliquez <a href="./admin.php?cat=upload&action=ajout">ici</a> pour ajouter une nouvelle image.</p>';
							echo '<p>Cliquez <a href="./admin.php?cat=upload">ici</a> pour revenir � la gestion des images.</p>';
						}
						else
						{
							echo '<h1>Echec de l\'upload</h1>';
							echo '<p>Cliquez <a href="./admin.php">ici</a> pour revenir � la page d\'accueil.</p>';
							echo '<p>Cliquez <a href="./admin.php?cat=upload&action=ajout">ici</a> pour ajouter une nouvelle image.</p>';
						}
					}
				break;
				case "del" :
					$confirm = intval($_GET['confirm']);
					$chemin = mysql_real_escape_string($_GET['chemin']);
					if ($confirm == 1)
					{
						// Supprime l'image de base
						$dossier_traite1 = './images/images/';
						$repertoire1 = opendir($dossier_traite1); //on d�finit le r�pertoire dans lequel on souhaite travailler
						while (false !== ($fichier1 = readdir($repertoire1))) //on lit chaque fichier du r�pertoire dans la boucle
						{
							//on met le chemin du fichier dans une variable simple
							$chemin1 = ''.$dossier_traite1.$chemin.'';
							//si le fichier n'est pas un r�pertoire
							if ($fichier1 != ".." AND $fichier1 != "." AND !is_dir($fichier1))
								{
									unlink($chemin1);
								}
						}
						closedir($repertoire1); //Ne pas oublier de fermer le dossier !EN DEHORS de la boucle ! Ce qui �vitera � PHP bcp de calculs et des pbs li�s � l'ouverture du dossier
						// Supprime la miniature
						$dossier_traite2 = './images/images/mini/';
						$repertoire2 = opendir($dossier_traite2);
						while (false !== ($fichier2 = readdir($repertoire2)))
						{
							$chemin2 = ''.$dossier_traite2.$chemin.'';
							if ($fichier2 != ".." AND $fichier2 != "." AND !is_dir($fichier2))
								{
									unlink($chemin2);
								}
						}
						closedir($repertoire2);
						// Supprime la redimension
						$dossier_traite3 = './images/images/redim/';
						$repertoire3 = opendir($dossier_traite3);
						while (false !== ($fichier3 = readdir($repertoire3)))
						{
							$chemin3 = ''.$dossier_traite3.$chemin.'';
							if ($fichier3 != ".." AND $fichier3 != "." AND !is_dir($fichier3))
								{
									unlink($chemin3);
								}
						}
						closedir($repertoire3);
						echo '<p>Vous avez bien supprim� la photo.</p>';
						echo '<p>Cliquez <a href="./admin.php">ici</a> pour revenir � la page d\'accueil.</p>';
						echo '<p>Cliquez <a href="./admin.php?cat=upload">ici</a> pour revenir � la gestion des images.</p>';
					}
					else
					{
						echo '<h1>Supprimer une image</h1>';
						echo 'Etes-vous s�r de vouloir supprimer cette image ? <a href="./admin.php?cat=upload&action=del&chemin='.$chemin.'&confirm=1">Oui</a> ou <a href="'.$page_precedente.'">Non</a><br /><br />';
						echo '<img src="./images/images/redim/'.$chemin.'" alt=""/><br />';
					}
				break;
				default :
					echo '<h1>Administration des Images</h1>';
					echo '<p>Les images ci-dessous sont h�berg�es sur le site. Vous pouvez r�cup�rer les <strong>url</strong> des images en cliquant dessus.</p>';
					echo '<h3>Ajouter une Image</h3>';
					echo '<p><a href="./admin.php?cat=upload&action=ajout">Ajouter une Image</a></p>';
					echo '<h3>Editer/Supprimer une Image</h3>';
					// On affiche toutes les images du dossier
					$dossier_traite = "./images/images/";
					$repertoire = opendir($dossier_traite);
					$i = 1;
					$j = 0;
					echo '<table id="album_images">';
					while (false !== ($fichier = readdir($repertoire)))
					{
						$chemin = ''.$dossier_traite.'mini/'.$fichier.'';
						if ($fichier != ".." AND $fichier != "." AND !is_dir($fichier) AND $fichier!= "mini" AND $fichier!= "redim" AND $fichier!= "index.php")
						{
							if ($i == 1) {echo '<tr>';} else{;}
							echo '<td><a href="./admin.php?cat=upload&action=voir&chemin='.$fichier.'"><img src="'.$chemin.'" alt=""/><br /><a href="./admin.php?cat=upload&action=del&chemin='.$fichier.'">Supprimer</a></td>';
							if ($i == 4) {echo '</tr>'; $i = 0;} else{;}
							$i++;
							$j++;
						}
					}
					if ($i == 1 AND $j == 0) {echo 'Il n\'y a pas d\'image sur le serveur.';}
					echo '</table>';
					closedir($repertoire);
					echo '<br /><a href="./admin.php">Retour</a>';
				}
			break;
			// ==========================================
			// Administration des pages
			// ==========================================
			case 'page' :
				switch ($choix)
				{
				case 'new':
					echo '<h3>Ajouter une Page</h3>';
					if (mysql_real_escape_string($_POST['confirm']=='ok')) {
						$titre=stripslashes(htmlspecialchars($_POST['titre']));
						$text=$_POST['editor'];
						$titre_de=stripslashes(htmlspecialchars($_POST['titre_de']));
						$text_de=$_POST['editor_de'];
						if ($_POST['choix2']=='' or $_POST['choix2_de']=='') {
							$cat_choix=mysql_real_escape_string($_POST['choix1']);
							// chopper le nom en de � partir de celui en fr
							$recup_nom_cat_de=mysql_query('SELECT categorie_de, categorie, categorie_ordre FROM pages WHERE categorie="'.$cat_choix.'"')or die(mysql_error());
							while ($nom_cat_de = mysql_fetch_array($recup_nom_cat_de)) {
								$cat_choix_de=$nom_cat_de['categorie_de'];
								$ordre_cat=$nom_cat_de['categorie_ordre'];
							}
							$possibilites_pages=mysql_query('SELECT DISTINCT ordre, categorie_ordre FROM pages WHERE categorie_ordre='.$ordre_cat.'')or die(mysql_error());
							$nb_pages=mysql_num_rows($possibilites_pages);
							$ordre_page=$nb_pages+1;
						} else {
							$cat_choix=mysql_real_escape_string($_POST['choix2']);
							$cat_choix_de=mysql_real_escape_string($_POST['choix2_de']);
							$possibilites_cat=mysql_query('SELECT DISTINCT categorie_ordre FROM pages')or die(mysql_error());
							$nb_cat=mysql_num_rows($possibilites_cat);
							$ordre_cat=$nb_cat+1;
							$ordre_page=1;
						}
						if (mb_strlen($titre)<5 or mb_strlen($titre_de)<5) {
							if (mb_strlen($titre)<5 or mb_strlen($titre_de)<5) {echo '<p>Le titre est trop court.</p>';}
							echo '<form method="post" action="./admin.php?cat=page&choix=new" enctype="multipart/form-data"><p>';
					 		echo '<p><label for="choix1">Choisissez la cat�gorie : </label>';
							echo '<select name="choix1" id="choix1">';
							$possibilites = mysql_query('SELECT DISTINCT categorie, categorie_de, categorie_ordre FROM pages ORDER BY categorie_ordre ASC')or die(mysql_error());
							$i=0;
							while ($choix = mysql_fetch_array($possibilites)) {
								if ($cat_choix==stripslashes(htmlspecialchars($choix['categorie']))) {$selected=' selected="selected"'; $i++;}
								else {$selected='';}
								echo $selected.'<br />';
								echo '<option'.$selected.' value="'.stripslashes(htmlspecialchars($choix['categorie'])).'">'.stripslashes(htmlspecialchars($choix['categorie'])).' <=> '.stripslashes(htmlspecialchars($choix['categorie_de'])).'</option>';
							}
							echo '</select> ';
							$nbre_cat=mysql_num_rows($possibilites);
							if ($nbre_cat>=0 && $nbre_cat<4) { // On limite � 4 le nbre de menus
								if ($i==0) {echo 'ou nommez-en une nouvelle :<br /><label for="choix2">en fran�ais : </label><input type="text" name="choix2" id="choix2" value="'.$cat_choix.'"/> <label for="choix2">et en allemand : </label><input type="text" name="choix2_de" id="choix2_de" value="'.$cat_choix_de.'"/><br />';}
								else {echo 'ou nommez-en une nouvelle :<br /><label for="choix2">en fran�ais : </label><input type="text" name="choix2" id="choix2" /> <label for="choix2">et en allemand : </label><input type="text" name="choix2_de" id="choix2_de" /><br />';}
							} else {echo '<br />';}
							// page en fran�ais
							echo '<p><label for="titre">Titre de la Page en Fran�ais : </label><input type="text" name="titre" id="titre" value="'.$titre.'"/></p>';
							echo '<textarea name="editor" id="editor">'.stripslashes($text).'</textarea>';
							include('./includes/config_toolbar.txt');
							echo '<br />';
							// page en allemand
							echo '<p><label for="titre_de">Titre de la Page en Allemand : </label><input type="text" name="titre_de" id="titre_de" value="'.$titre_de.'"/></p>';
							echo '<textarea name="editor_de" id="editor_de">'.stripslashes($text_de).'</textarea>';
							include('./includes/config_toolbar_de.txt');
							echo '<br /><input type="hidden" name="confirm" value="ok" />';
							echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
						} else {
							mysql_query('INSERT INTO pages VALUES ("'.mysql_real_escape_string($cat_choix).'", "'.mysql_real_escape_string($cat_choix_de).'", "'.$ordre_cat.'", "'.$ordre_page.'", "'.mysql_real_escape_string($titre).'", "'.mysql_real_escape_string($titre_de).'", "'.mysql_real_escape_string($text).'", "'.mysql_real_escape_string($text_de).'", "'.time().'")');
							echo '<p>La nouvelle a bien �t� enregistr�e.</p>';
						}
					} else {
						echo '<form method="post" action="./admin.php?cat=page&choix=new" enctype="multipart/form-data"><p>';
					 	// Si aucune cat�gorie, faire truc sp�cial !
						echo '<p><label for="choix1">Choisissez la cat�gorie : </label>';
						echo '<select name="choix1" id="choix1">';
						$possibilites = mysql_query('SELECT DISTINCT categorie, categorie_de, categorie_ordre FROM pages ORDER BY categorie_ordre ASC')or die(mysql_error());
						while ($choix = mysql_fetch_array($possibilites)) {
							echo '<option value="'.stripslashes(htmlspecialchars($choix['categorie'])).'">'.stripslashes(htmlspecialchars($choix['categorie'])).' <=> '.stripslashes(htmlspecialchars($choix['categorie_de'])).'</option>';
						}
						echo '</select> ';
						$nbre_cat=mysql_num_rows($possibilites);
						if ($nbre_cat>=0 && $nbre_cat<4) { // On limite � 4 le nbre de menus
							echo ' ou nommez-en une nouvelle :<br /><label for="choix2">en fran�ais : </label><input type="text" name="choix2" id="choix2" /> <label for="choix2">et en allemand : </label><input type="text" name="choix2_de" id="choix2_de" /><br />';
						} else {echo '<br />';}
						// page en fran�ais
						echo '<p><label for="titre">Titre de la Page en Fran�ais : </label><input type="text" name="titre" id="titre" /></p>';
						echo '<textarea name="editor" id="editor"></textarea>';
						include('./includes/config_toolbar.txt');
						echo '<br />';
						// page en allemand
						echo '<p><label for="titre_de">Titre de la Page en Allemand : </label><input type="text" name="titre_de" id="titre_de" /></p>';
						echo '<textarea name="editor_de" id="editor_de"></textarea>';
						include('./includes/config_toolbar_de.txt');
						echo '<br /><input type="hidden" name="confirm" value="ok" />';
						echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
					}
					echo '<br /><a href="./admin.php?cat=page">Retour</a>';
				break;
				case 'edit':
					echo '<h3>Editer une Page</h3>';
					if (mysql_real_escape_string($_POST['confirm']=='ok')) {
						$titre=stripslashes(htmlspecialchars($_POST['titre']));
						$text=$_POST['editor'];
						$titre_de=stripslashes(htmlspecialchars($_POST['titre_de']));
						$text_de=$_POST['editor_de'];
						/* if ($_POST['choix2']=='' or $_POST['choix2_de']=='') {
							$cat_choix=mysql_real_escape_string($_POST['choix1']);
							// chopper le nom en de � partir de celui en fr
							$recup_nom_cat_de=mysql_query('SELECT categorie_de, categorie, categorie_ordre FROM pages WHERE categorie="'.$cat_choix.'"')or die(mysql_error());
							while ($nom_cat_de = mysql_fetch_array($recup_nom_cat_de)) {
								$cat_choix_de=$nom_cat_de['categorie_de'];
								$ordre_cat=$nom_cat_de['categorie_ordre'];
							}
							$possibilites_pages=mysql_query('SELECT DISTINCT ordre, categorie_ordre FROM pages WHERE categorie_ordre='.$ordre_cat.'')or die(mysql_error());
							$nb_pages=mysql_num_rows($possibilites_pages);
							$ordre_page=$nb_pages+1;
						} else {
							$cat_choix=mysql_real_escape_string($_POST['choix2']);
							$cat_choix_de=mysql_real_escape_string($_POST['choix2_de']);
							$possibilites_cat=mysql_query('SELECT DISTINCT categorie_ordre FROM pages')or die(mysql_error());
							$nb_cat=mysql_num_rows($possibilites_cat);
							$ordre_cat=$nb_cat+1;
							$ordre_page=1;
						} */
						if (mb_strlen($titre)<5 or mb_strlen($titre_de)<5) {
							if (mb_strlen($titre)<5 or mb_strlen($titre_de)<5) {echo '<p>Le titre est trop court.</p>';}
							echo '<form method="post" action="./admin.php?cat=page&choix=edit&id='.$id.'" enctype="multipart/form-data"><p>';
							//
							// Difficile car il faut modifier l'ordre des autres pages encore !
							//
							// echo '<p><label for="choix1">Choisissez la cat�gorie : </label>';
							// echo '<select name="choix1" id="choix1">';
							// $possibilites = mysql_query('SELECT DISTINCT categorie, categorie_de, categorie_ordre FROM pages ORDER BY categorie_ordre ASC')or die(mysql_error());
							// $i=0;
							// while ($choix=mysql_fetch_array($possibilites)) {
								// if ($cat_choix==stripslashes(htmlspecialchars($choix['categorie']))) {$selected=' selected="selected"'; $i++;}
								// else {$selected='';}
								// echo $selected.'<br />';
								// echo '<option'.$selected.' value="'.stripslashes(htmlspecialchars($choix['categorie'])).'">'.stripslashes(htmlspecialchars($choix['categorie'])).' <=> '.stripslashes(htmlspecialchars($choix['categorie_de'])).'</option>';
							// }
							// echo '</select> ';
							// $nbre_cat=mysql_num_rows($possibilites);
							// if ($nbre_cat>=0 && $nbre_cat<4) { // On limite � 4 le nbre de menus
								// if ($i==0) {echo 'ou nommez-en une nouvelle :<br /><label for="choix2">en fran�ais : </label><input type="text" name="choix2" id="choix2" value="'.$cat_choix.'"/> <label for="choix2">et en allemand : </label><input type="text" name="choix2_de" id="choix2_de" value="'.$cat_choix_de.'"/><br />';}
								// else {echo 'ou nommez-en une nouvelle :<br /><label for="choix2">en fran�ais : </label><input type="text" name="choix2" id="choix2" /> <label for="choix2">et en allemand : </label><input type="text" name="choix2_de" id="choix2_de" /><br />';}
							// } else {echo '<br />';}
							// page en fran�ais
							echo '<p><label for="titre">Titre de la Page en Fran�ais : </label><input type="text" name="titre" id="titre" value="'.$titre.'"/></p>';
							echo '<textarea name="editor" id="editor">'.stripslashes($text).'</textarea>';
							include('./includes/config_toolbar.txt');
							echo '<br />';
							// page en allemand
							echo '<p><label for="titre_de">Titre de la Page en Allemand : </label><input type="text" name="titre_de" id="titre_de" value="'.$titre_de.'"/></p>';
							echo '<textarea name="editor_de" id="editor_de">'.stripslashes($text_de).'</textarea>';
							include('./includes/config_toolbar_de.txt');
							echo '<br /><input type="hidden" name="confirm" value="ok" />';
							echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
						} else {
							// mysql_query('UPDATE pages SET categorie="'.mysql_real_escape_string($cat_choix).'", categorie_de="'.mysql_real_escape_string($cat_choix_de).'", categorie_ordre="'.$ordre_cat.'", ordre="'.$ordre_page.'", titre="'.mysql_real_escape_string($titre).'", titre_de="'.mysql_real_escape_string($titre_de).'", contenu="'.$text.'", contenu_de="'.$text_de.'", edit="'.time().'" WHERE edit="'.$id.'"');
							mysql_query('UPDATE pages SET titre="'.mysql_real_escape_string($titre).'", titre_de="'.mysql_real_escape_string($titre_de).'", contenu="'.mysql_real_escape_string($text).'", contenu_de="'.mysql_real_escape_string($text_de).'", edit="'.time().'" WHERE edit="'.$id.'"');
							echo '<p>La nouvelle a bien �t� enregistr�e.</p>';
						}
					} else {
						echo '<form method="post" action="./admin.php?cat=page&choix=edit&id='.$id.'" enctype="multipart/form-data"><p>';
						// r�cup�rer les donn�es dans la bdd
						$requete = mysql_query('SELECT categorie, categorie_ordre, ordre, titre, titre_de, contenu, contenu_de, edit FROM pages WHERE edit='.$id.'') or die(mysql_error());
						$data = mysql_fetch_array($requete);
						//
						// Difficile car il faut modifier l'ordre des autres pages encore !
						//
						// echo '<p><label for="choix1">Choisissez la cat�gorie : </label>';
						// echo '<select name="choix1" id="choix1">';
						// $possibilites = mysql_query('SELECT DISTINCT categorie, categorie_de, categorie_ordre FROM pages ORDER BY categorie_ordre ASC')or die(mysql_error());
						// $i=0;
						// while ($choix=mysql_fetch_array($possibilites)) {
							// if (stripslashes(htmlspecialchars($data['categorie']))==stripslashes(htmlspecialchars($choix['categorie']))) {$selected=' selected="selected"'; $i++;}
							// else {$selected='';}
							// echo $selected.'<br />';
							// echo '<option'.$selected.' value="'.stripslashes(htmlspecialchars($choix['categorie'])).'">'.stripslashes(htmlspecialchars($choix['categorie'])).' <=> '.stripslashes(htmlspecialchars($choix['categorie_de'])).'</option>';
						// }
						// echo '</select> ';
						// $nbre_cat=mysql_num_rows($possibilites);
						// if ($nbre_cat>=0 && $nbre_cat<4) { // On limite � 4 le nbre de menus
							// echo ' ou nommez-en une nouvelle :<br /><label for="choix2">en fran�ais : </label><input type="text" name="choix2" id="choix2" /> <label for="choix2">et en allemand : </label><input type="text" name="choix2_de" id="choix2_de" /><br />';
						// } else {echo '<br />';}
						// page en fran�ais
						echo '<p><label for="titre">Titre de la Page en Fran�ais : </label><input type="text" name="titre" id="titre" value="'.stripslashes(htmlspecialchars($data['titre'])).'"/></p>';
						echo '<textarea name="editor" id="editor">'.stripslashes(htmlspecialchars($data['contenu'])).'</textarea>';
						include('./includes/config_toolbar.txt');
						echo '<br />';
						// page en allemand
						echo '<p><label for="titre_de">Titre de la Page en Allemand : </label><input type="text" name="titre_de" id="titre_de" value="'.stripslashes(htmlspecialchars($data['titre_de'])).'"/></p>';
						echo '<textarea name="editor_de" id="editor_de">'.stripslashes(htmlspecialchars($data['contenu_de'])).'</textarea>';
						include('./includes/config_toolbar_de.txt');
						echo '<br /><input type="hidden" name="confirm" value="ok" />';
						echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
					}
					echo '<a href="./admin.php?cat=page">Retour</a>';
				break;
				case 'move':
					echo '<h3>D�placer une Cat�gorie/Page</h3>';
					$where=mysql_real_escape_string($_GET['where']); // up=1 ou down=0
					$possibilites=mysql_query('SELECT DISTINCT categorie_ordre FROM pages ORDER BY categorie_ordre ASC')or die(mysql_error());
					$nb_cat=mysql_num_rows($possibilites);
					if ($id>0 && $id<=$nb_cat) { // id pour cat�gorie
						if ($id<$nb_cat && $where==0) { // on descend la cat�gorie
							mysql_query('UPDATE pages SET categorie_ordre=0 WHERE categorie_ordre='.$id.'');
							mysql_query('UPDATE pages SET categorie_ordre='.$id.' WHERE categorie_ordre='.($id+1).'');
							mysql_query('UPDATE pages SET categorie_ordre='.($id+1).' WHERE categorie_ordre=0');
							echo '<p>Le d�placement de la cat�gorie a r�ussi.</p>';
						} elseif ($id>1 && $where==1) { // on monte la cat�gorie
							mysql_query('UPDATE pages SET categorie_ordre=0 WHERE categorie_ordre='.$id.'');
							mysql_query('UPDATE pages SET categorie_ordre='.$id.' WHERE categorie_ordre='.($id-1).'');
							mysql_query('UPDATE pages SET categorie_ordre='.($id-1).' WHERE categorie_ordre=0');
							echo '<p>Le d�placement de la cat�gorie a r�ussi.</p>';
						} else {echo '<p>Le d�placement de la cat�gorie a �chou�.</p>';}
					} elseif ($id>1000000) { // id pour page
						$cherche=mysql_query('SELECT categorie_ordre, ordre, edit FROM pages WHERE edit='.$id.'')or die(mysql_error());
						while ($no=mysql_fetch_array($cherche)) { // ou utiliser mysql_result($cherche,0,0) et mysql_result($cherche,0,1)
							$cat_ordre=$no['categorie_ordre'];
							$ordre=$no['ordre'];
							break;
						}
						$nbre_pages=mysql_query('SELECT DISTINCT categorie_ordre, ordre FROM pages WHERE categorie_ordre='.$cat_ordre.'')or die(mysql_error());
						$nb_pages=mysql_num_rows($nbre_pages);
						if ($where==0) { // on descend la page
							if ($nb_pages==$ordre && $nb_cat==$cat_ordre) { // On ne peut pas descendre la page
								echo '<p>Il est impossible de d�placer plus bas la page.</p>';
							}
							elseif ($nb_pages==$ordre && $nb_cat>$cat_ordre) { // On descend la page d'une cat�gorie et on la met � la fin
								$pages_next=mysql_query('SELECT DISTINCT ordre, categorie_ordre, categorie, categorie_de FROM pages WHERE categorie_ordre='.($cat_ordre+1).'')or die(mysql_error());
								$nb_pages_next=mysql_num_rows($pages_next);
								mysql_query('UPDATE pages SET categorie="'.mysql_result($pages_next,0,2).'", categorie_de="'.mysql_result($pages_next,0,3).'", categorie_ordre='.($cat_ordre+1).', ordre='.($nb_pages_next+1).' WHERE edit='.$id.'')or die(mysql_error());
								// for ($i=1;$i<$nb_pages;$i++) { // remonte de 1 les autres pages
									// mysql_query('UPDATE pages SET ordre='.$i.' WHERE ordre='.($i+1).' AND categorie_ordre='.$cat_ordre.'')or die(mysql_error());
								// }
								echo '<p>Le d�placement de la page a r�ussi.</p>';
							}
							elseif ($nb_pages>=$ordre && $nb_cat>=$cat_ordre) { // on intervertit 2 pages
								mysql_query('UPDATE pages SET ordre=0 WHERE edit='.$id.'')or die(mysql_error());
								mysql_query('UPDATE pages SET ordre='.$ordre.' WHERE ordre='.($ordre+1).' AND categorie_ordre='.$cat_ordre.'')or die(mysql_error());
								mysql_query('UPDATE pages SET ordre='.($ordre+1).' WHERE ordre=0')or die(mysql_error());
								echo '<p>Le d�placement de la page a r�ussi.</p>';
							}
						}
						if ($where==1) { // on monte la page
							if (1==$ordre && 1==$cat_ordre) { // On ne peut pas monter la page
								echo '<p>Il est impossible de d�placer plus haut la page.</p>';
							}
							elseif (1==$ordre && 1<$cat_ordre) { // On monte la page d'une cat�gorie et on la met � la fin
								$pages_prev=mysql_query('SELECT DISTINCT ordre, categorie_ordre, categorie, categorie_de FROM pages WHERE categorie_ordre='.($cat_ordre-1).'')or die(mysql_error());
								$nb_pages_prev=mysql_num_rows($pages_prev);
								mysql_query('UPDATE pages SET categorie="'.mysql_result($pages_prev,0,2).'", categorie_de="'.mysql_result($pages_prev,0,3).'", categorie_ordre='.($cat_ordre-1).', ordre='.($nb_pages_prev+1).' WHERE edit='.$id.'');
								for ($i=1;$i<$nb_pages;$i++) { // remonte de 1 les autres pages
									mysql_query('UPDATE pages SET ordre='.$i.' WHERE ordre='.($i+1).' AND categorie_ordre='.$cat_ordre.'')or die(mysql_error());
								}
								echo '<p>Le d�placement de la page a r�ussi.</p>';
							}
							elseif ($nb_pages>=$ordre && $nb_cat>=$cat_ordre) { // on intervertit 2 pages
								mysql_query('UPDATE pages SET ordre=0 WHERE edit='.$id.'');
								mysql_query('UPDATE pages SET ordre='.$ordre.' WHERE ordre='.($ordre-1).' AND categorie_ordre='.$cat_ordre.'');
								mysql_query('UPDATE pages SET ordre='.($ordre-1).' WHERE ordre=0');
								echo '<p>Le d�placement de la page a r�ussi.</p>';
							}
						}
					} else {echo '<p>Le d�placement a �chou�.</p>';}
					//////////
					//////////
					// On remet une fois pour la facilit� mais dangereux si on actualise !
					// On peut aussi r�diriger vers la bonne page !
					//////////
					//////////
					echo '<p>Attention, n\'actualisez pas la page svp !</p>';
					$possibilites_cat=mysql_query('SELECT DISTINCT categorie_ordre FROM pages')or die(mysql_error());
					$nb_cat=mysql_num_rows($possibilites_cat);
					$possibilites_pages=mysql_query('SELECT DISTINCT ordre, categorie_ordre FROM pages WHERE categorie_ordre='.$nb_cat.'')or die(mysql_error());
					$nb_pages=mysql_num_rows($possibilites_pages);
					$i=0;
					$j=0;
					$k=0;
					$les_pages = mysql_query('SELECT categorie, categorie_de, categorie_ordre, ordre, titre, titre_de, edit FROM pages ORDER BY categorie_ordre ASC, ordre ASC')or die(mysql_error());
					// prendre en compte 2 cas : si on a 1 cat�gorie et 1 page
					while ($titre = mysql_fetch_array($les_pages)) {
						if ($titre['ordre']==1 && $titre['categorie_ordre']==1 && $nb_cat==1 && $nb_pages==1) {$lien_page='<li><em>'.htmlspecialchars(stripslashes($titre['titre'])).' <=> '.htmlspecialchars(stripslashes($titre['titre_de'])).'</em> - <a href="./admin.php?cat=page&choix=edit&id='.$titre['edit'].'">�diter</a> ou <a href="./admin.php?cat=page&choix=del&id='.$titre['edit'].'">supprimer</a></li>';
						} elseif ($titre['ordre']==1 && $titre['categorie_ordre']==1) {$lien_page='<li><em>'.htmlspecialchars(stripslashes($titre['titre'])).' <=> '.htmlspecialchars(stripslashes($titre['titre_de'])).'</em> - <a href="./admin.php?cat=page&choix=edit&id='.$titre['edit'].'">�diter</a> ou <a href="./admin.php?cat=page&choix=del&id='.$titre['edit'].'">supprimer</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['edit'].'&where=0">down</a></li>';
						} elseif ($titre['ordre']==$nb_pages && $titre['categorie_ordre']==$nb_cat) {$lien_page='<li><em>'.htmlspecialchars(stripslashes($titre['titre'])).' <=> '.htmlspecialchars(stripslashes($titre['titre_de'])).'</em> - <a href="./admin.php?cat=page&choix=edit&id='.$titre['edit'].'">�diter</a> ou <a href="./admin.php?cat=page&choix=del&id='.$titre['edit'].'">supprimer</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['edit'].'&where=1">up</a></li>';
						} else {$lien_page='<li><em>'.htmlspecialchars(stripslashes($titre['titre'])).' <=> '.htmlspecialchars(stripslashes($titre['titre_de'])).'</em> - <a href="./admin.php?cat=page&choix=edit&id='.$titre['edit'].'">�diter</a> ou <a href="./admin.php?cat=page&choix=del&id='.$titre['edit'].'">supprimer</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['edit'].'&where=1">up</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['edit'].'&where=0">down</a></li>';}
						if ($last_cat==htmlspecialchars(stripslashes($titre['categorie'])) || $j==0) {
							if ($i==0) {
								$last_cat=htmlspecialchars(stripslashes($titre['categorie']));
								if ($titre['categorie_ordre']==1 && $nb_cat==1) {echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong><ul>';
								} elseif ($titre['categorie_ordre']==1) {echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=0">down</a><ul>';
								} else {echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=1">up</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=0">down</a><ul>';}
								echo $lien_page;
								$i++;
								$j++;
							} elseif ($i!=0) {
								$last_cat=htmlspecialchars(stripslashes($titre['categorie']));
								echo $lien_page;
							}
						} elseif ($titre['categorie_ordre']==$nb_cat) {
							$last_cat=htmlspecialchars(stripslashes($titre['categorie']));
							echo '</ul></li>';
							echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=1">up</a><ul>';
							echo $lien_page;
						} else {
							$last_cat=htmlspecialchars(stripslashes($titre['categorie']));
							echo '</ul></li>';
							echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=1">up</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=0">down</a><ul>';
							echo $lien_page;
						}
						$k++;
					}
					if ($i!=0) {echo '</ul></li>';}
					if ($k==0) {echo'<p>Aucune Page cr�e.</p>';}
					//////////
					//////////
					echo '<a href="./admin.php?cat=page">Retour</a>';
				break;
				case 'del':
					echo '<h3>Supprimer une Page</h3>';
					if (mysql_real_escape_string($_GET['confirm'])=="ok") {
						echo '<p>Vous avez supprime la Page.</p>';
						mysql_query('DELETE FROM pages WHERE edit = "'.$id.'"')or die(mysql_error());
						} else {
						echo '<p>Etes-vous s�r de supprimer cette Page ? <a href="./admin.php?cat=page&choix=del&id='.$id.'&confirm=ok">OUI</a></p>';
						}
					echo '<a href="./admin.php?cat=page">Retour</a>';
				break;
				default :
					echo '<h1>Administration des Pages</h1>';
					echo '<h3>Ajouter une Page</h3>';
					echo '<p><a href="./admin.php?cat=page&choix=new">Ajouter une Page</a></p>';
					echo '<h3>Editer/Supprimer une Page</h3>';
					$possibilites_cat=mysql_query('SELECT DISTINCT categorie_ordre FROM pages')or die(mysql_error());
					$nb_cat=mysql_num_rows($possibilites_cat);
					// Ceci en dessous me donne un truc inutile...
					// aaaaaaaaaaaaa
					// aaaaaaaaaaaaa
					// corriger d�bugger ...
					$possibilites_pages=mysql_query('SELECT DISTINCT ordre, categorie_ordre FROM pages WHERE categorie_ordre='.$nb_cat.'')or die(mysql_error());
					$nb_pages=mysql_num_rows($possibilites_pages);
					$i=0;
					$j=0;
					$k=0;
					$les_pages = mysql_query('SELECT categorie, categorie_de, categorie_ordre, ordre, titre, titre_de, edit FROM pages ORDER BY categorie_ordre ASC, ordre ASC')or die(mysql_error());
					// prendre en compte 2 cas : si on a 1 cat�gorie et 1 page
					while ($titre = mysql_fetch_array($les_pages)) {
						if ($titre['ordre']==1 && $titre['categorie_ordre']==1 && $nb_cat==1 && $nb_pages==1) {$lien_page='<li><em>'.htmlspecialchars(stripslashes($titre['titre'])).' <=> '.htmlspecialchars(stripslashes($titre['titre_de'])).'</em> - <a href="./admin.php?cat=page&choix=edit&id='.$titre['edit'].'">�diter</a> ou <a href="./admin.php?cat=page&choix=del&id='.$titre['edit'].'">supprimer</a></li>';
						} elseif ($titre['ordre']==1 && $titre['categorie_ordre']==1) {$lien_page='<li><em>'.htmlspecialchars(stripslashes($titre['titre'])).' <=> '.htmlspecialchars(stripslashes($titre['titre_de'])).'</em> - <a href="./admin.php?cat=page&choix=edit&id='.$titre['edit'].'">�diter</a> ou <a href="./admin.php?cat=page&choix=del&id='.$titre['edit'].'">supprimer</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['edit'].'&where=0">down</a></li>';
						} elseif ($titre['ordre']==$nb_pages && $titre['categorie_ordre']==$nb_cat) {$lien_page='<li><em>'.htmlspecialchars(stripslashes($titre['titre'])).' <=> '.htmlspecialchars(stripslashes($titre['titre_de'])).'</em> - <a href="./admin.php?cat=page&choix=edit&id='.$titre['edit'].'">�diter</a> ou <a href="./admin.php?cat=page&choix=del&id='.$titre['edit'].'">supprimer</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['edit'].'&where=1">up</a></li>';
						} else {$lien_page='<li><em>'.htmlspecialchars(stripslashes($titre['titre'])).' <=> '.htmlspecialchars(stripslashes($titre['titre_de'])).'</em> - <a href="./admin.php?cat=page&choix=edit&id='.$titre['edit'].'">�diter</a> ou <a href="./admin.php?cat=page&choix=del&id='.$titre['edit'].'">supprimer</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['edit'].'&where=1">up</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['edit'].'&where=0">down</a></li>';}
						if ($last_cat==htmlspecialchars(stripslashes($titre['categorie'])) || $j==0) {
							if ($i==0) {
								$last_cat=htmlspecialchars(stripslashes($titre['categorie']));
								if ($titre['categorie_ordre']==1 && $nb_cat==1) {echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong><ul>';
								} elseif ($titre['categorie_ordre']==1) {echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=0">down</a><ul>';
								} else {echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=1">up</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=0">down</a><ul>';}
								echo $lien_page;
								$i++;
								$j++;
							} elseif ($i!=0) {
								$last_cat=htmlspecialchars(stripslashes($titre['categorie']));
								echo $lien_page;
							}
						} elseif ($titre['categorie_ordre']==$nb_cat) {
							$last_cat=htmlspecialchars(stripslashes($titre['categorie']));
							echo '</ul></li>';
							echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=1">up</a><ul>';
							echo $lien_page;
						} else {
							$last_cat=htmlspecialchars(stripslashes($titre['categorie']));
							echo '</ul></li>';
							echo '<li><strong>'.htmlspecialchars(stripslashes($titre['categorie'])).' <=> '.htmlspecialchars(stripslashes($titre['categorie_de'])).'</strong> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=1">up</a> - <a href="./admin.php?cat=page&choix=move&id='.$titre['categorie_ordre'].'&where=0">down</a><ul>';
							echo $lien_page;
						}
						// autre id�e
						// if ($k==0) {$cat_ordre_remember=$titre['categorie_ordre'];}
						// elseif ($k!=0 && $cat_ordre_remember==$titre['categorie_ordre']) {$next_cat=false;}
						// else {$next_cat=true;}
						$k++;
					}
					if ($i!=0) {echo '</ul></li>';}
					if ($k==0) {echo'<p>Aucune Page cr��e.</p>';}
					echo '<a href="./admin.php">Retour</a>';
				}
			break;
			
			// ==========================================
			// Attention, mesure de s�curit�,
			// tout le monde peut acc�der aux fichiers qui suivent !!!
			// DANGEREUX --> corriger
			// ==========================================
			
			// ==========================================
			// Administration des Ev�nements
			// ==========================================
			case 'calendrier' :
				include('./calendrier_admin.php');
			break;
			// ==========================================
			// Administration des Fichiers pour les h�berger
			// ==========================================
			case 'fichier':
				include('./upload.php');
			break;
			// ==========================================
			// Administration de la Newsletter
			// ==========================================
			case 'newsletter':
				include('./newsletter_admin.php');
			break;
			// ==========================================
			// Par d�faut
			// ==========================================
			default:
			echo '<p><a href="./admin.php?cat=news">Administration des Nouvelles</a></p>';
			echo '<p><a href="./admin.php?cat=calendrier">Administration des Ev�nements</a></p>';
			echo '<p><a href="./admin.php?cat=page">Administration des Pages</a></p>';
			echo '<p><a href="./admin.php?cat=upload">H�berger des Images</a></p>';
			echo '<p><a href="./admin.php?cat=fichier">H�berger des Fichiers</a></p>';
			echo '<p><a href="./admin.php?cat=newsletter">Administration de la Newsletter</a></p>';
		}
	} else { // utilisateur non connect�
		echo '<h1>Administration du site Internet</h1>';
		if (isset($_POST['action'])) {$action = mysql_real_escape_string($_POST['action']);} else {$action='';}
		if ($action=='connect') {
			$login = stripslashes(htmlspecialchars($_POST['login']));
			$pass = stripslashes(htmlspecialchars($_POST['pass'])); // crypter le mot de passe � faire
			$requete = mysql_query('SELECT login, pass FROM admin WHERE login="'.$login.'"') or die(mysql_error());
			$data = mysql_fetch_array($requete);
			// g�n�ration du token
			$nb=16; // nb de caract�res
			$chaine="ABCDEFGHIJKLMNOPQRSTUVWXYZ23456789"; // pas de 0 ni de 1
			$nb_caract=$nb;
			$token='';
			for($u=1; $u<=$nb_caract; $u++) {
				$nb=strlen($chaine);
				$nb=mt_rand(0,($nb-1));
				$token.=$chaine[$nb];
			}
			if ($login==$data['login'] and $pass==$data['pass'] and mb_strlen($login)>1 and mb_strlen($pass)>1) {
				$expire = time()+3600; //  1 heure de conservation des cookies stock� dans la bdd afin de le comparer avec le time
				setcookie('token', $token, $expire);
				// mysql_query('INSERT INTO session VALUES ("'.$token.'", "'.$_SERVER['REMOTE_ADDR'].'", '.$expire.') ON DUPLICATE KEY UPDATE time = '.$expire.'');
				mysql_query('INSERT INTO session VALUES ("'.$token.'", "'.$_SERVER['HTTP_X_FORWARDED_FOR'].'", '.$expire.') ON DUPLICATE KEY UPDATE time = '.$expire.'');
				echo '<p>Connecte --> <a href="./admin.php">actualiser</a></p>';
				// echo 'utilisateur ip : '.$_SERVER['REMOTE_ADDR'].'<br />';
				// echo 'serveur ip : '.$_SERVER['SERVER_ADDR'];
				// $_SERVER['HTTP_X_FORWARDED_FOR'] car ya proxy � l'agep...
			} else {
			echo '<p>Pas reussi a se connecter !</p>';
			echo '<p><a href="./admin.php">actualiser</a></p>';
			}
		} else { ?>
			<form method="post" action="./admin.php" enctype="multipart/form-data"><p>
			<label for="login">Login<br /></label><input type="text" name="login" id="login" /><br />
			<label for="pass">Mot de Passe<br /></label><input type="password" name="pass" id="pass" /><br />
			<input type="hidden" name="action" value="connect" />
			<input type="submit" value="Se connecter" id="connect"/></p></form>
		<?php
		}
	}
	?>
	</div> <!-- fermeture div contenu -->
<?php include('./includes/bas_page.php'); ?>