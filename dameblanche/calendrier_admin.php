<?php

// Inclus dans la page admin.php
// variables $choix et $id

switch ($choix) {
	case 'new':
		echo '<h3>Ajouter un Ev�nement</h3>';
		if (mysql_real_escape_string($_POST['confirm']=='ok')) {
			$titre=stripslashes(htmlspecialchars($_POST['titre']));
			$text=$_POST['editor'];
			$titre_de=stripslashes(htmlspecialchars($_POST['titre_de']));
			$text_de=$_POST['editor_de'];
			$date=stripslashes(htmlspecialchars($_POST['date']));
			$heure=stripslashes(htmlspecialchars($_POST['heure']));
			$erreur_nb=0;
			// V�rification du titre
			if (mb_strlen($titre)<5 or mb_strlen($titre_de)<5) {
				$erreur_nb=$erreur_nb+1;
				$erreur.='<li>Le titre est trop court.</li>';
			}
			// V�rification de la date
			$donnee = explode("/",$date);
			if (strlen($donnee[0]) == 1) {$donnee[0] = '0'.$donnee[0];}
			if (!isset($donnee[0]) || !isset($donnee[1]) || !isset($donnee[2]) || $donnee[0] == 'JJ' || $donnee[1] == 'MM' || $donnee[2] == 'AAAA' || strlen($donnee[0]) != 2 || strlen($donnee[1]) != 2 || strlen($donnee[2]) != 4 ) {
				$jour=date('d');
				$mois=date('m');
				$annee=date('Y');
				$erreur_nb=$erreur_nb+1;
				$erreur.='<li>Erreur dans la forme de la date.</li>';
			} else {
				$jour=$donnee[0];
				$mois=$donnee[1];
				$annee=$donnee[2];
			}
			// V�rification de l'heure
			$donnee1 = explode(":", $heure);
			if (!isset($donnee1[0]) || !isset($donnee1[1]) || $donnee1[0] == 'hh' || $donnee1[1] == 'mm' || strlen($donnee1[0]) != 2 || strlen($donnee1[1]) != 2 ) {
				$heure=date('H');
				$minute=date('i');
				$erreur_nb=$erreur_nb+1;
				$erreur.='<li>Erreur dans la forme de l\'heure.</li>';
			} else {
				$heure=$donnee1[0];
				$minute=$donnee1[1];
				$memo_date = $annee.$mois.$jour.$heure.$minute;
			}
			if ($erreur_nb>0) {
				echo '<p><ul>'.$erreur.'</ul></p>';
				echo '<form method="post" action="./admin.php?cat=calendrier&choix=new" enctype="multipart/form-data"><p>';
				echo '<p><label for="titre">Titre de l\'Ev�nement en Fran�ais : </label><input type="text" name="titre" id="titre" value="'.$titre.'"/></p>';
				echo '<textarea name="editor" id="editor">'.$text.'</textarea>';
				include('./includes/config_toolbar.txt');
				echo '<p><label for="titre_de">Titre de l\'Ev�nement en Allemand : </label><input type="text" name="titre_de" id="titre_de" value="'.$titre_de.'"/></p>';
				echo '<textarea name="editor_de" id="editor_de">'.$text_de.'</textarea>';
				include('./includes/config_toolbar_de.txt');
				echo '<br /><label for="date">Date de votre Ev�nement : </label><input type="text" name="date" id="date" value ="'.$jour.'/'.$mois.'/'.$annee.'" /> (JJ/MM/AAA) <br />';
				echo '<label for="heure">Heure de votre Ev�nement : </label><input type ="text" name="heure" id="heure" value="'.$heure.':'.$minute.'" /> (hh:mm) <br />';
				echo '<br /><input type="hidden" name="confirm" value="ok" />';
				echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
			} else {
			mysql_query('INSERT INTO calendrier (titre, contenu, titre_de, contenu_de, minute, heure, jour, mois, annee, memo_date)
			VALUES ("'.$titre.'" , "'.$text.'", "'.$titre_de.'" , "'.$text_de.'", "'.$minute.'", "'.$heure.'", "'.$jour.'", "'.$mois.'", "'.$annee.'", "'.$memo_date.'") ') or die(mysql_error());
			echo '<p>L\'Ev�nement a bien �t� enregistr�.</p>';
			}
		} else {
			echo '<form method="post" name="form" action="./admin.php?cat=calendrier&choix=new" enctype="multipart/form-data">';
			// page en fran�ais
			echo '<p><label for="titre">Titre de l\'Ev�nement en Fran�ais : </label><input type="text" name="titre" id="titre" /></p>';
			echo '<textarea name="editor" id="editor"></textarea>';
			include('./includes/config_toolbar.txt');
			// page en allemand
			echo '<p><label for="titre_de">Titre de l\'Ev�nement en Allemand : </label><input type="text" name="titre_de" id="titre_de" /></p>';
			echo '<textarea name="editor_de" id="editor_de"></textarea>';
			include('./includes/config_toolbar_de.txt');
			echo '<br /><label for="date">Date de votre Ev�nement : </label><input type="text" name="date" id="date" value ="'.date('d/m/Y').'" /> (JJ/MM/AAA) <br />';
			echo '<label for="heure">Heure de votre Ev�nement : </label><input type ="text" name="heure" id="heure" value="'.date('H:i').'" /> (hh:mm) <br />';
			echo '<br /><input type="hidden" name="confirm" value="ok" />';
			echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
		}
		echo '<a href="./admin.php?cat=calendrier">Retour</a>';
	break;
	case 'edit':
		echo '<h3>Editer un Ev�nement</h3>';
		if (mysql_real_escape_string($_POST['confirm']=='ok')) {
			$id=$_POST['id'];
			$titre=stripslashes(htmlspecialchars($_POST['titre']));
			$text=$_POST['editor'];
			$titre_de=stripslashes(htmlspecialchars($_POST['titre_de']));
			$text_de=$_POST['editor_de'];
			$date=stripslashes(htmlspecialchars($_POST['date']));
			$heure=stripslashes(htmlspecialchars($_POST['heure']));
			$erreur_nb=0;
			// V�rification du titre
			if (mb_strlen($titre)<5 or mb_strlen($titre_de)<5) {
				$erreur_nb=$erreur_nb+1;
				$erreur.='<li>Le titre est trop court.</li>';
			}
			// V�rification de la date
			$donnee = explode("/",$date);
			if (strlen($donnee[0]) == 1) {$donnee[0] = '0'.$donnee[0];}
			if (!isset($donnee[0]) || !isset($donnee[1]) || !isset($donnee[2]) || $donnee[0] == 'JJ' || $donnee[1] == 'MM' || $donnee[2] == 'AAAA' || strlen($donnee[0]) != 2 || strlen($donnee[1]) != 2 || strlen($donnee[2]) != 4 ) {
				$jour=date('d');
				$mois=date('m');
				$annee=date('Y');
				$erreur_nb=$erreur_nb+1;
				$erreur.='<li>Erreur dans la forme de la date.</li>';
			} else {
				$jour=$donnee[0];
				$mois=$donnee[1];
				$annee=$donnee[2];
			}
			// V�rification de l'heure
			$donnee1 = explode(":", $heure);
			if (!isset($donnee1[0]) || !isset($donnee1[1]) || $donnee1[0] == 'hh' || $donnee1[1] == 'mm' || strlen($donnee1[0]) != 2 || strlen($donnee1[1]) != 2 ) {
				$heure=date('H');
				$minute=date('i');
				$erreur_nb=$erreur_nb+1;
				$erreur.='<li>Erreur dans la forme de l\'heure.</li>';
			} else {
				$heure=$donnee1[0];
				$minute=$donnee1[1];
				$memo_date = $annee.$mois.$jour.$heure.$minute;
			}
			if ($erreur_nb>0) {
				echo '<p><ul>'.$erreur.'</ul></p>';
				echo '<form method="post" action="./admin.php?cat=calendrier&choix=edit" enctype="multipart/form-data"><p>';
				echo '<p><label for="titre">Titre de l\'Ev�nement en Fran�ais : </label><input type="text" name="titre" id="titre" value="'.$titre.'"/></p>';
				echo '<textarea name="editor" id="editor">'.$text.'</textarea>';
				include('./includes/config_toolbar.txt');
				echo '<p><label for="titre_de">Titre de l\'Ev�nement en Allemand : </label><input type="text" name="titre_de" id="titre_de" value="'.$titre_de.'"/></p>';
				echo '<textarea name="editor_de" id="editor_de">'.$text_de.'</textarea>';
				include('./includes/config_toolbar_de.txt');
				echo '<br /><label for="date">Date de votre Ev�nement : </label><input type="text" name="date" id="date" value ="'.$jour.'/'.$mois.'/'.$annee.'" /> (JJ/MM/AAA) <br />';
				echo '<label for="heure">Heure de votre Ev�nement : </label><input type ="text" name="heure" id="heure" value="'.$heure.':'.$minute.'" /> (hh:mm) <br />';
				echo '<br /><input type="hidden" name="confirm" value="ok" />';
				echo '<br /><input type="hidden" name="id" value="'.$id.'" />';
				echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
			} else {
			mysql_query('UPDATE calendrier SET titre="'.mysql_real_escape_string($titre).'", contenu="'.$text.'", titre_de="'.mysql_real_escape_string($titre_de).'", contenu_de="'.$text_de.'", minute="'.$minute.'", heure="'.$heure.'", jour="'.$jour.'", mois="'.$mois.'", annee="'.$annee.'", memo_date="'.$memo_date.'" WHERE memo_date="'.$id.'"') or die(mysql_error());
			echo '<p>L\'Ev�nement a bien �t� enregistr�.</p>';
			}
		} else {
			echo '<form method="post" name="form" action="./admin.php?cat=calendrier&choix=edit" enctype="multipart/form-data">';
			// r�cup�rer les donn�es dans la bdd
			$requete = mysql_query('SELECT titre, contenu, titre_de, contenu_de, minute, heure, jour, mois, annee, memo_date FROM calendrier WHERE memo_date='.$id.'') or die(mysql_error());
			$data = mysql_fetch_array($requete);
			// page en fran�ais
			echo '<p><label for="titre">Titre de l\'Ev�nement en Fran�ais : </label><input type="text" name="titre" id="titre" value="'.$data['titre'].'"/></p>';
			echo '<textarea name="editor" id="editor">'.$data['contenu'].'</textarea>';
			include('./includes/config_toolbar.txt');
			// page en allemand
			echo '<p><label for="titre_de">Titre de l\'Ev�nement en Allemand : </label><input type="text" name="titre_de" id="titre_de" value="'.$data['titre_de'].'"/></p>';
			echo '<textarea name="editor_de" id="editor_de">'.$data['contenu_de'].'</textarea>';
			include('./includes/config_toolbar_de.txt');
			echo '<br /><label for="date">Date de votre Ev�nement : </label><input type="text" name="date" id="date" value ="'.$data['jour'].'/'.$data['mois'].'/'.$data['annee'].'" /> (JJ/MM/AAA) <br />';
			echo '<label for="heure">Heure de votre Ev�nement : </label><input type ="text" name="heure" id="heure" value="'.$data['heure'].':'.$data['minute'].'" /> (hh:mm) <br />';
			echo '<br /><input type="hidden" name="confirm" value="ok" />';
			echo '<br /><input type="hidden" name="id" value="'.$id.'" />';
			echo '<input type="submit" value="Enregistrer" id="save"/></p></form>';
		}
		echo '<a href="./admin.php?cat=calendrier">Retour</a>';
	break;
	case 'del':
	echo '<h3>Supprimer un Ev�nement</h3>';
		if (mysql_real_escape_string($_GET['confirm'])=="ok") {
		mysql_query('DELETE FROM calendrier WHERE memo_date = "'.$id.'"')or die(mysql_error());
		echo '<p>Vous avez supprime l\'Ev�nement.</p>';
			} else {
			echo '<p>Etes-vous s�r de supprimer cet Ev�nement ? <a href="./admin.php?cat=calendrier&choix=del&id='.$id.'&confirm=ok">OUI</a></p>';
		}
		echo '<a href="./admin.php?cat=calendrier">Retour</a>';
	break;
	default:
	echo '<h1>Administration des Ev�nements</h1>';
	echo '<h3>Ajouter un Ev�nement</h3>';
	echo '<a href="./admin.php?cat=calendrier&choix=new">Ajouter un Ev�nement</a>';
	echo '<h3>Publier/Editer/Supprimer un Ev�nement</h3>';
	$requete = mysql_query('SELECT titre, titre_de, memo_date FROM calendrier ORDER BY memo_date DESC') or die(mysql_error());
	$i=0;
	while ($data = mysql_fetch_array($requete)) {
		echo '<p>'.stripslashes(htmlspecialchars($data['titre'])).' <=> '.stripslashes(htmlspecialchars($data['titre_de'])).'<br />';
		echo '<a href="./admin.php?cat=calendrier&choix=edit&id='.$data['memo_date'].'">Editer</a> ';
		echo '<a href="./admin.php?cat=calendrier&choix=del&id='.$data['memo_date'].'">Supprimer</a></p>';
		$i++;
	}
	if ($i==0) {echo'<p>Aucun Ev�nement cr��.</p>';}
	echo '<a href="./admin.php">Retour</a>';
}
?>